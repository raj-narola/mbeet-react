var axios = require('axios')
var axiosDefaults = require("axios/lib/defaults");
axiosDefaults.xsrfCookieName = "csrftoken"
axiosDefaults.xsrfHeaderName = "X-CSRFToken"
axios.defaults.headers.post['Content-Type'] = 'application/json'

export const initUnit = () => {
    return {
        type: 'INIT_UNIT',
        payload: new Promise((resolve, reject) => resolve(1))
    }
}

export const getUnits=() => {
    return dispatch => {
        return axios.get('/units/getUnits/',{})
        .then(resp => {
            return dispatch({
                type: 'FETCH_UNITS',
                resp,
            })
        })
    }
}

export const addUnit = (values) => {
    return dispatch => {
        console.log("add unit action",values)
        var body = values
       return axios.post('/units/addUnit/'
        ,{
            body
        })
         /*   .then(resp => {
                //console.log(resp)
                return dispatch({
                    type: 'REGISTER_USER',
                    resp,
                })
            })
            .catch(error => {
                //console.log(resp)
                return dispatch({
                    type: 'REGISTER_USER',
                    error,
                })
            })*/
    }
}


export const deleteUnit= (unitid) => {
    return dispatch => {
        return axios.post('/units/deleteUnit/'
    ,{
        unitid
    })
    .then(()=> {

        return dispatch({
            type: 'DELETE_UNIT',
            unitid
        })
    }

    )
    }
}



export const getUnit=(unitid) => {
    return dispatch => {
        console.log("inside getUnit action",unitid)
        return axios.post('/units/getUnit/',{ unitid })
        .then(resp => {
            return dispatch({
                type: 'FETCH_UNIT',
                resp,
            })
        })
    }
}



export const updateUnit=(unitid,values) => {
    return dispatch => {
        console.log("inside updateUnit action",unitid,values)
        return axios.post('/units/updateUnit/',{ unitid,values })
    }
}