var axios = require('axios')
var axiosDefaults = require("axios/lib/defaults");
axiosDefaults.xsrfCookieName = "csrftoken"
axiosDefaults.xsrfHeaderName = "X-CSRFToken"
axios.defaults.headers.post['Content-Type'] = 'application/json'


export const getUser = () => {

    return dispatch => {

        // return {
        //     type: 'LOGIN_USER',
        //     payload: handleAPI(values)
        // }

        return axios.get('/accounts/getUser/', {
            
        })
        .then(resp => {
            return dispatch({
                type: 'FETCH_USER',
                resp,
            })
        })

       /* .catch(error => {
            //console.log(error)
            return dispatch({
                type: 'LOGIN_USER',
                error,
            })
        })*/
    }
}



export const addUser = (values) => {
    var axiosDefaults = require("axios/lib/defaults");
    axiosDefaults.xsrfCookieName = "csrftoken"
    axiosDefaults.xsrfHeaderName = "X-CSRFToken"
    axios.defaults.headers.post['Content-Type'] = 'application/json'
    return dispatch => {
        console.log("add user action",values)
        var body = values
        return axios.post('/accounts/addUser/'
        ,{
            body
        })
         /*   .then(resp => {
                //console.log(resp)
                return dispatch({
                    type: 'REGISTER_USER',
                    resp,
                })
            })
            .catch(error => {
                //console.log(resp)
                return dispatch({
                    type: 'REGISTER_USER',
                    error,
                })
            })*/
    }
}


export const deleteUser = (userid) => {
    return dispatch => {
        console.log("user delete",userid)
        return axios.post('/accounts/deleteUser/'
        ,{
            userid
        })
        .then(() => {
            return dispatch({
                type: 'DELETE_USER',
                userid
            })
        })
    }

}


export const getShowUser = (userid) => {
    return dispatch => {
        console.log("show user ",userid)
        return axios.post('/accounts/getShowUser/',{ userid })
        .then(resp => {
            console.log(resp.data['users'][0])
            return dispatch({
                type: 'SHOW_USER',
                resp,
            })
        })
    }
}


export const editUser = (userid,values) => {
    var axiosDefaults = require("axios/lib/defaults");
    axiosDefaults.xsrfCookieName = "csrftoken"
    axiosDefaults.xsrfHeaderName = "X-CSRFToken"
    axios.defaults.headers.post['Content-Type'] = 'application/json'
    return dispatch => {
        console.log("editUser action",values)
        var body = values
        return axios.post('/accounts/editUser/'
        ,{
            body,userid
        })
         /*   .then(resp => {
                //console.log(resp)
                return dispatch({
                    type: 'REGISTER_USER',
                    resp,
                })
            })
            .catch(error => {
                //console.log(resp)
                return dispatch({
                    type: 'REGISTER_USER',
                    error,
                })
            })*/
    }
}


export const getUserUnits = (userid) => {
    return dispatch => {
        return axios.post('/accounts/getUserUnits/', {
            userid            
        })
        .then(resp => {
            if(resp.data['units']==null)
            {
                return dispatch({
                    type: 'NO_GET_USER_UNIT',
                })
            }
            else{
                return dispatch({
                    type: 'GET_USER_UNIT',
                    resp
                })
                
            }
            
        })

}
}