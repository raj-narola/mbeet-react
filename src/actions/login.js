var axios = require('axios')
var axiosDefaults = require("axios/lib/defaults");
axiosDefaults.xsrfCookieName = "csrftoken"
axiosDefaults.xsrfHeaderName = "X-CSRFToken"
axios.defaults.headers.post['Content-Type'] = 'application/json'


function handleAPI(values) {
    console.log("In")
    return axios.post('/login1/', {
        values
    })
}

export const loginUser = (values) => {

    return dispatch => {

        // return {
        //     type: 'LOGIN_USER',
        //     payload: handleAPI(values)
        // }

        return axios.post('/login1/', {
            values
        })
        .then(resp => {
            //console.log(resp)
            const password = values.password
            return dispatch({
                type: 'LOGIN_USER',
                resp,
                password
            })
        })

        .catch(error => {
            //console.log(error)
            return dispatch({
                type: 'LOGIN_USER',
                error,
            })
        })
    }
}

export const initAuth = () => {
    return {
        type: 'INIT_AUTH',
        payload: new Promise((resolve, reject) => resolve(1))
    }
}

export const registerUser = (values) => {
    
    return dispatch => {
        return axios.post('/accounts/registerUser/', {
            values
        })
            .then(resp => {
                //console.log(resp)
                return dispatch({
                    type: 'REGISTER_USER',
                    resp,
                })
            })
            .catch(error => {
                //console.log(resp)
                return dispatch({
                    type: 'REGISTER_USER',
                    error,
                })
            })
    }
}