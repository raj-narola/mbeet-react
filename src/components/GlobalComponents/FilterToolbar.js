import React, {Component} from 'react';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';




// Style
import './css/FilterToolbar.css'

class FilterToolbar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: props.rowShowing,
      filters: parseInt(props.currentFilter),
    };
    this.handleChangeFilter = this.handleChangeFilter.bind(this)

  }

  handleChange = (event, index, value) => {
    this.setState({value: value});
    this.props.onchange(value)
  }
  handleChangeFilter = (event, index, value) => {
    const {filters, onChangeFilter} = this.props
    this.setState({filters: value})
    onChangeFilter(filters[value].params,filters[value].value, value)
  }

  render() {
    const {showList, onSearch, filters, onChangeFilter, search, customFilter} = this.props

    return (
          <div>
            </div>
    );
  }
}

FilterToolbar.defaultProps = {
  customFilter: () => {}
}

export default FilterToolbar
