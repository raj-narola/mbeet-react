import React from 'react'
import FilterToolbar from './FilterToolbar'
import Pagination from './Pagination'
import RowTopDestinationList from '../Pages/TopDestinations/indexTopDestinations/rowTopDestinationList/RowTopDestinationList'
import DummyRowTopDestinationList from '../Pages/TopDestinations/indexTopDestinations/rowTopDestinationList/DummyRowTopDestinationList'
import Loading from './Loading'

function showing(total, current_page, display_count){

  const start = (current_page === 1) ? current_page : ((current_page * display_count) - display_count)
  const minus = (total - ((current_page - 1) * display_count))
  const end = (minus > display_count) ? (current_page * display_count) : total
  let result = ''
  result += 'Showing '
  result += (start > total) ? total : start
  result += ' to '+end
  result += ' of '
  result += total+' entries'
  return result
}

const TopDestinationsForList = (props) => {
  const {topDestinations, onDelete, loading, update, showList, errorMsg, rowShowing, onChangeShowing, permissions} = props
  return (
    <div>
      {!loading && <FilterToolbar
                        rowShowing={rowShowing}
                        onchange={onChangeShowing}
                        showList={showList}
                        onSearch={props.onSearch}
                        filters={props.filters}
                        onChangeFilter={props.onChangeFilter}
                        currentFilter={props.currentFilter}
                        search={props.search} />}

      {/* Units List content */}
      <div className="page-container units-container">
        {(loading || update) && <Loading error={errorMsg} />}
        {(loading || errorMsg) ? (
          <div className="card-deck">
            <DummyRowTopDestinationList />
            <DummyRowTopDestinationList />
            <DummyRowTopDestinationList />
            <DummyRowTopDestinationList />
          </div>
        ):(
          <div className="card-deck">

            {topDestinations.map(topDestination => {
              return (<RowTopDestinationList
                onDelete={onDelete} key={'topDestination'+topDestination.id} topDestination={topDestination} permissions={permissions}
            />)
            })}
          </div>
        )}

      </div>

      {!loading &&
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-6">
            <span className="showing-entries">
            {showing(props.total, props.page, props.rowShowing)}
            </span>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6">
            <Pagination
              onPageChange={props.onPageChange}
              total={props.total}
              show={props.rowShowing}
              currentPage={props.page} />
          </div>
        </div>
      }

    </div>
  )
}
export default TopDestinationsForList
