import PagesContainer from './PagesContainer'
import Breadcrumb from './Breadcrumb'
import PageHeader from './PageHeader'
import TableList from './TableList'
import Loading from './Loading'
import AlertMsg from './AlertMsg'
import FlatsList from './FlatsList'
export {
    PagesContainer,
    Breadcrumb,
    PageHeader,
    TableList,
    Loading,
    AlertMsg,
    FlatsList
}
