/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

import { TextValidator, SelectValidator} from 'react-material-ui-form-validator'


class Step2 extends Component{
  constructor(props){
    super(props)

    this.state = {
      loading: false,
      unit_status: false,
      unit_enabled: false,
      nos: 1,
      sub_unit: null,
      form: {
        sub_unit: 0,
      },
    }

    this.handleChangeField = this.handleChangeField.bind(this)
  }

  handleSelectUnitType  = (event, index, value) => {
    this.setState({unit_type: value})
    this.props.onChange('unit_type', value)
  }
  handleSelectUnitClass  = (event, index, value) => {
    this.setState({unit_class: value})
    this.props.onChange('unit_class', value)
  }
  hanldeUnitStatus = (event, isInputChecked) => {
    this.setState({unit_status: isInputChecked})
    this.props.onChange('unit_status', isInputChecked)
  }
  hanldeUnitEnabled = (event, isInputChecked) => {
    this.setState({unit_enabled: isInputChecked})
    this.props.onChange('unit_enabled', isInputChecked)
  }
  handleSelectSubUnit = (event, index, value) => {
    this.setState({sub_unit: value})
    this.props.onChange('sub_unit', value)
  }


  handleChangeField = (event) => {
    const { form } = this.state;
    form[event.target.id] = event.target.value
    this.setState({ form })
    this.props.onChange(event.target.id, event.target.value)
  }

  componentWillMount(){
    const {formType, unit} = this.props
    if(formType === 'edit'){
      this.setState({
        unit_type: unit.unit_type,
        unit_class: unit.unit_class.toUpperCase(),
        unit_status: unit.unit_status,
        unit_enabled: unit.unit_enabled,
        nos: unit.number_of_subunits,
        form: {
          sub_unit: false,
          price: unit.price,
          total_area: unit.total_area,
        },
      })
      this.props.onChange('unit_type', unit.unit_type)
      this.props.onChange('unit_class', unit.unit_class.toUpperCase())
      this.props.onChange('unit_status', unit.unit_status)
      this.props.onChange('unit_enabled', unit.unit_enabled)
      this.props.onChange('sub_unit', unit.number_of_subunits)
      this.props.onChange('price', unit.price)
      this.props.onChange('total_area', unit.total_area)
    }
  }

  subunitsShow = () => {
    const {form, nos} = this.state
    if(!form.sub_unit) return nos - 1

    return form.sub_unit
  }

  subunitsSend = () => {
    const {form, nos}    = this.state

    if(!form.sub_unit) return (nos === 1) ? 0 : nos

    if(parseInt(form.sub_unit) === 0) return 0

    return parseInt(form.sub_unit) + 1

  }

  render(){
    const {display, permissions} = this.props
    const {form} = this.state

    return(
      <div style={{display: (display ? 'block' : 'none')}}>

        {permissions &&
          permissions.unit_permissions.publish &&
          <div className="form-field">
            <Toggle
              label="Published"
              defaultToggled={this.state.unit_status}
              onToggle={this.hanldeUnitStatus}
              />
              <input type="hidden" name="unit_status" value={this.state.unit_status} />
          </div>
        }
        <div className="form-field">
          <Toggle
            label="User Enabled"
            defaultToggled={this.state.unit_enabled}
            onToggle={this.hanldeUnitEnabled}
            />
            <input type="hidden" name="unit_enabled" value={this.state.unit_enabled} />
        </div>

      {/* Price* */}
      <div className="form-field">

        <TextValidator
            floatingLabelText="Price"
            onChange={this.handleChangeField}
            name="price"
            id="price"
            min={0}
            type="number"
            value={form.price}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />
      </div>


      {/* Unit type */}
      <div className="form-field">

        <SelectValidator
           floatingLabelText="Select Unit Type"
           value={this.state.unit_type}
           onChange={this.handleSelectUnitType}
           name="unit_type"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={['this field is required']}
         >

           <MenuItem value={1} primaryText="Resort" />
           <MenuItem value={2} primaryText="Villa" />
           <MenuItem value={3} primaryText="Apartment" />
           <MenuItem value={4} primaryText="Shaleeh" />
           <MenuItem value={5} primaryText="Private Room" />
           <MenuItem value={6} primaryText="Shared Room" />
         </SelectValidator>
         <input type="hidden" name="unit_type" value={this.state.unit_type} />
      </div>

      {/* Unit Class */}
      <div className="form-field">

        <SelectValidator
           floatingLabelText="Unit Class"
           value={this.state.unit_class}
           onChange={this.handleSelectUnitClass}
           name="unit_class"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={['this field is required']}
         >

           <MenuItem value="A" primaryText="A" />
           <MenuItem value="B" primaryText="B" />
           <MenuItem value="C" primaryText="C" />
         </SelectValidator>
         <input type="hidden" name="unit_class" value={this.state.unit_class} />
      </div>

      {/* Total Area */}
      <div className="form-field">

        <TextValidator
            floatingLabelText="Total Area"
            onChange={this.handleChangeField}
            id="total_area"
            name="total_area"
            value={form.total_area}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />
      </div>

      <div className="form-field">
        <TextValidator
            floatingLabelText="Number of Subunits"
            onChange={this.handleChangeField}
            name="sub_unit_fake"
            id="sub_unit"
            type="number"
            min="0"
            value={this.subunitsShow()}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />
          <input type="hidden" name="number_of_subunits" value={this.subunitsSend()}
          />
      </div>


      </div>
    )
  }

}

export default Step2
