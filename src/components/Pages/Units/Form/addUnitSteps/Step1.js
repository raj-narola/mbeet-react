/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
import RaisedButton from 'material-ui/RaisedButton';

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { TextValidator, SelectValidator, AutoCompleteValidator} from 'react-material-ui-form-validator'

import AutoComplete from 'material-ui/AutoComplete';

import SimpleMap from './maps'

class Step1 extends Component{

  constructor(props){
    super(props)
    this.state = {
      // user_id: '',
      // searchText: '',
      form: {},
    }

    this.handleChangeField = this.handleChangeField.bind(this)
   /* this.handleChangeUserID = this.handleChangeUserID.bind(this)*/
  }
 /* handleChangeUserID(user){

    this.setState({user_id: user.id, searchText: user.name});
  this.props.onChange('user_id', user.id)
}*/

  handleChangeUser = (s, dataSource) => {
    const obj = dataSource.filter(d => d.name === s)[0]
    if(obj) this.setState({user_id: obj.id})
    else this.setState({user_id: ''})
    this.props.onChange('user_id', null)
  }

  handleChangeField(event){

    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
    this.props.onChange(event.target.name, event.target.value)
    //console.log("step1---- ",this.props)

  }

  componentWillMount(){
    const {formType, unit} = this.props
    if(formType === 'edit'){
      this.setState({
        user_id: unit.owner_name,
        searchText: unit.owner_name,
        form: {
          title_en: unit.title_en,
          title_ar: unit.title_ar,
          body_en: unit.body_en,
          body_ar: unit.body_ar,
        }
      })
      this.props.onChange('user_id', unit.owner_name)
      this.props.onChange('title_en', unit.title_en)
      this.props.onChange('title_ar', unit.title_ar)
      this.props.onChange('body_en', unit.body_en)
      this.props.onChange('body_ar', unit.body_ar)
    }
  }

  render(){
    const {users, display, unit} = this.props

    const {form} = this.state
    return(
      <div style={{display: (display ? 'block' : 'none')}}>
        {/* Select User */}
        {/* {users &&
          <div className="form-field">

            <AutoCompleteValidator
              floatingLabelText="Owner"
              filter={AutoComplete.caseInsensitiveFilter}
              dataSource={users}
              dataSourceConfig={ {text: 'name', value: 'id'}  }
              value={this.state.user_id}
              searchText={this.state.searchText}
              openOnFocus={true}
              fullWidth={true}
              onNewRequest={this.handleChangeUserID}
              onUpdateInput={this.handleChangeUser}
              name="user_id"
              validators={['required']}
              errorMessages={['this field is required']}
            />
            <input  name="user_id" value={this.state.user_id} type="text" className="hidden-input" />


        </div> } */}

        {/* Title English */}
        <div className="form-field">

         <TextValidator
             floatingLabelText="Title English"
             onChange={this.handleChangeField}
             name="title_en"
             value={form.title_en}
             style={{width: '100%'}}
             validators={['required']}
             errorMessages={['this field is required']}
           />

       </div>

         {/* Title Arabic */}
         <div className="form-field">

           <TextValidator
               floatingLabelText="Title Arabic"
               onChange={this.handleChangeField}
               name="title_ar"
               value={form.title_ar}
               style={{width: '100%'}}
               validators={['required']}
               errorMessages={['this field is required']}
             />
          </div>

        {/* Description English*/}
        <div className="form-field">

        <TextValidator
            floatingLabelText="Description English"
            onChange={this.handleChangeField}
            name="body_en"
            value={form.body_en}
            multiLine={true}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />

       </div>

        {/* Description Arabic */}
        <div className="form-field">

        <TextValidator
            floatingLabelText="Description Arabic"
            onChange={this.handleChangeField}
            name="body_ar"
            value={form.body_ar}
            multiLine={true}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />

        </div>

      </div>
    )
  }
}
export default Step1
