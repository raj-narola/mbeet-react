import React from 'react'

const AddImgBtn = (props) => {

  return (
    <div className="add-img">
      <div className="add-image-border">
        <span>Add Images</span>
        <i className="fa fa-plus" aria-hidden="true"></i>
        <input multiple type="file" name="file" onChange={props.onSelectImg} />
      </div>
    </div>
  )
}

export default AddImgBtn
