import React from 'react'
import {Tabs, Tab} from 'material-ui/Tabs';
import AssignmentIndIcon       from 'material-ui/svg-icons/action/assignment-ind';
import LocationCity       from 'material-ui/svg-icons/social/location-city'
import UnitInfo from './tabs/UnitInfo'
import DisabledUnits from './tabs/DisabledUnits'

const UnitDetailes = (props) => {
  const {unit} = props
  return (
    <div>
      <Tabs className="tabs">

        <Tab
          icon={<LocationCity className="tab-icon" />}
          label="Unit Info">

          <div className="tab-content">
            <UnitInfo unit={unit} />
          </div>

        </Tab>

        <Tab
          icon={<AssignmentIndIcon className="tab-icon" />}
          label={'Disabled Units (0)'}>

          <div className="tab-content">
            {/* <DisabledUnits unit={unit} /> */}
          </div>
        </Tab>

      </Tabs>
    </div>
  )
}
export default UnitDetailes
