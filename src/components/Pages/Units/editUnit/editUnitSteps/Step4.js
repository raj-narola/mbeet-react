/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { TextValidator, SelectValidator} from 'react-material-ui-form-validator'


class Step4 extends Component{

  constructor(props){
    super(props)
    this.state = {
      available_as: props.unit.available_as,
      bed_type: props.unit.bed_type,
    }
  }

  handleSelectAvailbleAs  = (event, index, value) => this.setState({available_as: value})
  handleSelectBedType     = (event, index, value) => this.setState({bed_type: value})
  render(){
    const {display, unit} = this.props
    return(
      <div style={{display: (display ? 'block' : 'none')}}>
      {/* Unit Available As */}
      <div className="form-field">
        <SelectField
           floatingLabelText="Unit Available As"
           value={this.state.available_as}
           onChange={this.handleSelectAvailbleAs}
           name="available_as"
           style={{width: '100%'}}
         >
           <MenuItem value={1} primaryText="Daily" />
           <MenuItem value={2} primaryText="Weekly" />
           <MenuItem value={3} primaryText="Monthly" />
           <MenuItem value={4} primaryText="Max 3 months" />
         </SelectField>
         <input type="hidden" name="available_as" value={this.state.available_as} />
      </div>


      {/* Number of Guests */}
      <div className="form-field">
          <TextField
           hintText="Number of Guests"
           style={{width: '100%'}}
           name="number_of_guests"
           type="number"
           defaultValue={unit.number_of_guests}
         />
      </div>

      {/* Number of Rooms */}
      <div className="form-field">
          <TextField
          defaultValue={unit.number_of_rooms}
           hintText="Number of Rooms"
           style={{width: '100%'}}
           name="number_of_rooms"
           type="number"
         />
      </div>

      {/* Number of Beds */}
      <div className="form-field">
          <TextField
            defaultValue={unit.number_of_beds}
            hintText="Number of Beds"
            style={{width: '100%'}}
            name="number_of_beds"
            type="number"
         />
      </div>

      {/* Number of Baths */}
      <div className="form-field">
          <TextField
           hintText="Number of Baths"
           style={{width: '100%'}}
           name="number_of_baths"
           type="number"
           defaultValue={unit.number_of_baths}
         />
      </div>

      <div className="form-field">
        <SelectValidator
           floatingLabelText="Bed Type"
           value={this.state.bed_type}
           onChange={this.handleSelectBedType}
           name="bed_type"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={['this field is required']}
         >
           <MenuItem value={1} primaryText="Single" />
           <MenuItem value={2} primaryText="Double" />
         </SelectValidator>
         <input type="hidden" name="bed_type" value={this.state.bed_type} />
      </div>




      </div>
    )
  }
}

export default Step4
