/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
import AddImgBtn from '../../addUnit/addUnitSteps/AddImgBtn'
/*import {imagePath, pathToUrl, filePreview} from '../../../../../Utilities/cloudinaryUpload'*/
import {Loading} from '../../../../GlobalComponents/GlobalComponents'

// External Packages
import $ from 'jquery'

class Step6 extends Component{
  constructor(props){
    super(props)
    this.state = {
      loading: false,
    }
    this.handleUploadImgs = this.handleUploadImgs.bind(this)
    this.onRemoveNewImg = this.onRemoveNewImg.bind(this)
  }

  onRemoveNewImg(index){
    const {handleImages} = this.props
    const images = this.props.images
    images.splice(index, 1)
    handleImages(images)
    this.forceUpdate()
  }



  handleUploadImgs(e){

    let input = e.target
    const files = input.files // get files
    const {handleImages} = this.props // fo setState on EditUnit Component

    // stop do anyting if the user click Cancel Btn
    if (files.length === 0) return false

    this.setState({loading: true}) // Start Loading

    let images = this.props.images // get images array from EditUnit Component

    filePreview(files)
    .then(imgs => {
      this.setState({loading: false}) // Stop Loading
      imgs.map(img => {
        images.push(img)
        handleImages(images)
        this.forceUpdate()
      })

      input.value = null // reset file input
    })
  }


  render(){
    const {display, unit, images} = this.props

    return (
      <div style={{display: (display ? 'block' : 'none')}}>
        {this.state.loading && <Loading />}
        <div className="container" style={{padding: '50px 0'}}>
          <div className="row">
            {unit.images.map((img, index) => {
              return (
                <div key={'img'+index} className="col-lg-3 col-md-3 col-sm-4 col-image-preview">
                  <span className="delete-prev-img" onClick={() => this.props.onRemoveImg(img.id)}>x</span>
                  <a href={img.url} target="_blank">
                    <img src={img.url} className="rounded img-fluid preview-img" />
                  </a>

                </div>
              )
            })}

            {images.map((img, index) => {
              return (
                <div key={'img'+index} className="col-lg-3 col-md-3 col-sm-4 col-image-preview">
                  <span className="delete-prev-img" onClick={() => this.onRemoveNewImg(index)}>x</span>
                  <img src={img} className="rounded img-fluid preview-img" />
                </div>
              )
            })}

            <div className="col-lg-3 col-md-3 col-sm-4">
              <AddImgBtn onSelectImg={this.handleUploadImgs} />
            </div>
          </div>
        </div>
      </div>
    )
  }

}

export default Step6
