/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';

// Material UI Icons
import DirectionsCarIcon from 'material-ui/svg-icons/maps/directions-car'
import NetworkWifiIcon from 'material-ui/svg-icons/device/network-wifi'
import PetsIcon from 'material-ui/svg-icons/action/pets'
import AcUnitIcon from 'material-ui/svg-icons/places/ac-unit'
import SmokingRoomsIcon from 'material-ui/svg-icons/places/smoking-rooms'
import RestaurantIcon from 'material-ui/svg-icons/maps/restaurant'
import PersonalVideoIcon from 'material-ui/svg-icons/notification/personal-video'
import WhatshotIcon from 'material-ui/svg-icons/social/whatshot'
import PoolIcon from 'material-ui/svg-icons/places/pool'

import SupervisorAccountIcon from 'material-ui/svg-icons/action/supervisor-account'
import LocalDiningIcon from 'material-ui/svg-icons/maps/local-dining'
import DevicesIcon from 'material-ui/svg-icons/device/devices'
import CachedIcon from 'material-ui/svg-icons/action/cached'
import StreetviewIcon from 'material-ui/svg-icons/maps/streetview'
import FilterTiltShiftIcon from 'material-ui/svg-icons/image/filter-tilt-shift'

// External Packages
import $ from 'jquery'

class Step5 extends Component{
  constructor(props){
    super(props)

    this.state = {
      rules: [],
      amenities: [],
    }

    this.rules = [
      'Not suitable for pets',
      'No parties or events',
      'Check in is anytime after 4 PM',
      'Please warn the host if something is broken or damaged'
    ]

    this.handleAmenities = this.handleAmenities.bind(this)
  }



  componentWillMount(){


    // Set rules
    let rules = []
    this.props.unit.rules.map(rule => rules.push(rule.id))

    // Set amenities
    let amenities = []
    this.props.unit.amenities.map(amenity => amenities.push(amenity.id))

    // put to state
    this.setState({rules,amenities})


  }

  handleSelectRules  = (event, index, value) => this.setState({rules: value})
  handleSelectAmenities = (event, index, amenities) => this.setState({amenities});

  handleAmenities(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({amenities : this.state.amenities.filter(an => an != parseInt(e.target.value))})
      obj.parent().removeClass('active')
    }else{
      this.setState({amenities : [...this.state.amenities, parseInt(e.target.value)]})
      obj.parent().addClass('active')
    }


  }

  amenitiesIcons(name){
    switch(name){
      case 'Wi-fi':
        return (<NetworkWifiIcon />)

      case 'Swimming pool':
        return (<PoolIcon />)

      case 'Kitchen':
        return (<RestaurantIcon />)

      case 'Parking':
        return (<DirectionsCarIcon />)

      case 'Parking':
        return (<DirectionsCarIcon />)

      case 'Reception service':
        return (<SupervisorAccountIcon />)

      case 'Food & bavarage':
        return (<LocalDiningIcon />)

      case 'Media & technology':
        return (<DevicesIcon />)

      case 'Cleaning service':
        return (<CachedIcon />)

      case 'Business facilities':
        return (<StreetviewIcon />)

      case 'Outdoor activities':
        return (<FilterTiltShiftIcon />)

      default :
        return (<DirectionsCarIcon />)
    }
  }


  render(){
    const {display, rules, amenities} = this.props
    return (
      <div style={{display: (display ? 'block' : 'none')}}>
        {/* Rules */}
        {rules &&
        <div className="form-field">
          <SelectField
              multiple={true}
              floatingLabelText="Rules"
              value={this.state.rules}
              onChange={this.handleSelectRules}
              name="available_as"
              style={{width: '100%'}}>
              {rules.map(rule => {
                return (<MenuItem
                            key={'rul'+rule.name}
                            value={rule.id}
                            primaryText={rule.name}
                            checked={this.state.rules.includes(rule.id)} />)
              })}


           </SelectField>

           {this.state.rules.map(rule => {
             return (<input key={rule} type="hidden" name="rule_ids[]" value={rule} />)
           })}


        </div> }

        {amenities &&
        <div className="form-field clearfix">
          <div>
            <label>Amenities</label>
          </div>
          <div className="row">
            {amenities.map(amenity => {
              const active = (this.state.amenities.includes(amenity.id)) ? true : false
              return (
                <div key={amenity.name} className="col-lg-2 col-md-2">
                  <Checkbox
                    checkedIcon={this.amenitiesIcons(amenity.name)}
                    uncheckedIcon={this.amenitiesIcons(amenity.name)}
                    label={amenity.name}
                    className={'amenity-item '+(active ? 'active': '')}
                    value={amenity.id}
                    onClick={this.handleAmenities}
                    name="amenity"
                    defaultChecked={active}
                  />
                </div>
              )
            })}
            {this.state.amenities.map(amenity => {
              return (<input key={amenity} type="hidden" name="amenity_ids[]" value={amenity} />)
            })}

            {this.state.amenities.length === 0 && <input type="hidden" name="amenity_ids[]" value=" " />}


          </div>

        </div> }
      </div>
    )
  }

}

export default Step5
