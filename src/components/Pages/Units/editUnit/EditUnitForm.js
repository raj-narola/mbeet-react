/*
* EditUnitForm: Compnent
* Child of: EditUnit
*/

// Main Packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

// Components
import Step1 from './editUnitSteps/Step1'
import Step2 from './editUnitSteps/Step2'
import Step3 from './editUnitSteps/Step3'
import Step4 from './editUnitSteps/Step4'
import Step5 from './editUnitSteps/Step5'
import Step6 from './editUnitSteps/Step6'
// External Packages
//import serializeForm from 'form-serialize'

// Style
import '../addUnit/addUnitStyle.css'

import { ValidatorForm} from 'react-material-ui-form-validator'


// Material UI
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

import {
  Step,
  Stepper,
  StepButton,
} from 'material-ui/Stepper';


class EditUnitForm extends Component {

  constructor(props){
    super(props)

    this.state = {
      users: null,
      room_type: null,
      unit_type: null,
      available_as: null,
      city: null,
      rules: [],
      amenities: [],
      stepIndex: 0,
    }
  }


  handleNext = () => {
    const {stepIndex} = this.state;
    if (stepIndex < 6) {
      this.setState({stepIndex: stepIndex + 1});
    }
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };


  // render
  render(){

    const {stepIndex} = this.state;
    const contentStyle = {margin: '0 16px'};
    const {unt, users, cities, rules, amenities} = this.props

    return(
      <ValidatorForm onSubmit={this.props.handleSubmit} className="add-unit-form">

      <Stepper linear={false} activeStep={stepIndex}>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 0})}>
            Unit Information
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 1})}>
            Unit Details
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 2})}>
            Location
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 3})}>
            Rooms Details
          </StepButton>
        </Step>

        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 4})}>
            Rules and Amenities
          </StepButton>
        </Step>

        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 5})}>
            Images
          </StepButton>
        </Step>


      </Step>
      </Stepper>
      <div style={contentStyle}>

        <Step1 unit={unt} users={users} display={stepIndex === 0 ? true : false} />
        <Step2 unit={unt} display={stepIndex === 1 ? true : false} />
        <Step3 unit={unt} cities={cities} display={stepIndex === 2 ? true : false} />
        <Step4 unit={unt} display={stepIndex === 3 ? true : false} />
        <Step5 unit={unt} rules={rules} amenities={amenities} display={stepIndex === 4 ? true : false} />
        <Step6
          unit={unt}
          onRemoveImg={this.props.onRemoveImg}
          images={this.props.images}
          handleImages={this.props.handleImages}
          display={stepIndex === 5 ? true : false} />
        <Step7 unit={unt} cities={cities} display={stepIndex === 6 ? true : false} />


          <div style={{marginTop: 12}}>
            <FlatButton
              label="Back"
              disabled={stepIndex === 0}
              onTouchTap={this.handlePrev}
              style={{marginRight: 12}}
            />
            {stepIndex < 5 &&

              <RaisedButton
                label="Next"
                disabled={stepIndex === 5}
                primary={true}
                onTouchTap={this.handleNext}
              />}

            {stepIndex === 5 &&
              <RaisedButton
                label="Submit"

                primary={true}
                type="submit"
              />}
          </div>
      </div>


      </ValidatorForm>
    )
  }

}
export default EditUnitForm
