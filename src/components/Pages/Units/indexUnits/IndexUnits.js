/*
* IndexUsers - Component connected to redux
*/

// import main packages
import React, {Component} from 'react'
import {connect} from 'react-redux'
import SweetAlert from 'sweetalert-react'
import { renderToStaticMarkup } from 'react-dom/server';
/*import DisableUnitForm from './DisableUnitForm'*/
/*import EnableUnitForm from './EnableUnitForm'*/
import {handleResponseErr} from '../../../../Utilities/functions'
import {deleteUnit} from '../../../../actions/Unit';

// Units Redux Actions
/*import {
  getUnits,
  deleteUnit,
  restoreUnit,
  unitsSearch,
  enableUnit} from '../../../../Redux/actions/unitsActions'*/

/*import { getCurrentUserPermission } from '../../../../Redux/actions/usersActions'*/
import {getUnits} from '../../../../actions/Unit'
import {deleteItem} from '../../../../Utilities/functions'
// Material Icon
import LocationCity       from 'material-ui/svg-icons/social/location-city'
import Avatar       from 'material-ui/Avatar';

// Components
import RowUnit from './rowUnit/RowUnit'

import swal from 'sweetalert'

import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  FilterToolbar,
  FlatsList
  } from '../../../GlobalComponents/GlobalComponents'

  
// External Packages
import { CSSTransitionGroup } from 'react-transition-group'


class IndexUnits extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      showList : [
        {count: 8, name: '8'},
        {count: 12, name: '12'},
        {count: 24, name: '24'},
      ],
      filters: [
        {value: 0, params: 0, name: 'All'},
        {value: 'true',params: 'unit_status', name: 'Published'},
        {value: 'false', params: 'unit_status', name: 'Waiting for approve'},
        {value: 'true', params: 'soft_delete', name: 'Deleted'},
        {value: 'false', params: 'soft_delete', name: 'Undeleted'},
        {value: 'true', params: 'unit_enabled', name: 'Enabled'},
        {value: 'false', params: 'unit_enabled', name: 'Disabled'},
      ],
      comID: 'units',
      currentFilter: 0,
      currentPage: 1,
      showUnits: 8,
      search: '',
      disable_unit: false,
    }

    this.handleDelete = this.handleDelete.bind(this)
   /* this.handleOnChangeShowing = this.handleOnChangeShowing.bind(this)
    this.handlePagination = this.handlePagination.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleRestore = this.handleRestore.bind(this)
    this.handleFilters = this.handleFilters.bind(this)
    this.handleSearch = this.handleSearch.bind(this)

    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)*/
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  /***************************
  * On Click Delete
  * @id : (Intger)
  ****************************/
  handleDelete(e, id){
    console.log("id in handleDelete",id)
    e.preventDefault()
    const _this = this
    const {dispatch} = this.props
    /*deleteItem(id,dispatch,deleteUnit, "Unit has some associated so can't be deleted", _this, 'Yes, delete it!', 'Moved to Trash')*/
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: 'Yes, Delete it!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    () => {
      dispatch(deleteUnit(id))
      .then(() => {
        swal({
          title: 'Deleted',
          type: 'success',
          text: "deleted",
          timer: 2000,
          showConfirmButton: false
        })
        // console.log("after delete")
        // dispatch(getUnits())
      })
      .catch(err => {
        _this.forceUpdate()
        swal("Error", err.response.data.message, "error");
      })
    })
   
   
  }

  /***************************
  * On Change Filter
  ****************************/
 /* handleFilters(filter_type, value, index){
    const {dispatch} = this.props
    const {filters} = this.state
    this.setState({currentFilter: index})
    this.setSession('currentPage', 1)
    this.setSession('currentFilter', index)
    dispatch(getUnits(
      this.state.showUnits, // limit
      0, // offset
      filters[index],// filter
      'id', //sort by
      'asc', // sort direction
      this.state.search,
    ))
  }*/

  /***************************
  * On search
  ****************************/
  /*handleSearch(e){
    const {dispatch} = this.props
    const length = e.target.value.length
    const s = e.target.value

    const {filters} = this.state

    if(length >= 3 || length === 0){
      dispatch(getUnits(
        this.getSession('showUnits'),
        0,
        filters[this.getSession('currentFilter')],
        'id',
        'asc',
        s
      ))
      this.setSession('search', s)
      this.setSession('currentPage', 1)
    }
  }*/

  /***************************
  * On Click Restore
  * @id : (Intger)
  ****************************/
  /*handleRestore(e, id){
    e.preventDefault()
    const {dispatch} = this.props

    dispatch(restoreUnit(id)).then(() => {
      this.forceUpdate()
      swal({
        title: "Restored Successfully",
        type: "success",
        timer: 2000,
        showConfirmButton: false
      })

    })

  }*/

  /***************************
  * On Click Disable
  * @id : (Intger)
  ****************************/
  /*onDisable = (e, unit_id, subunits) => {
    e.preventDefault()
    this.setState({disable_unit: true, unit_id, subunits})
  }*/

  /***************************
  * On Click Enable
  * @id : (Intger)
  ****************************/

  /*onEnable = (e, unit_id, subunits) => {
    e.preventDefault()
    this.setState({enable_unit: true, unit_id, subunits})
  }*/

  // 

  componentWillMount(){
    const {dispatch} = this.props
    dispatch(getUnits())
   /* this.loadList()*/
  
  }

  
  /***************************
  * On change showing users
  ****************************/
  /*handleOnChangeShowing(count){
    const {dispatch} = this.props
    const {filters, currentFilter, search} = this.state
    dispatch(getUnits(
      count, // limit
      0, // offset
      filters[currentFilter], // filter
      'id', // sort by
      'asc', // sort direction
      search,
    )).then(() => {
      this.setState({showUnits: count, currentPage: 1})

      this.setSession('showUnits', count)
      this.setSession('currentPage', 1)
    })
  }*/


  /***************************
  * On Click Pagination
  ****************************/
  /*handlePagination(e, page){
    e.preventDefault()
    const {filters, currentFilter, search} = this.state
    if(page === this.state.currentPage) return false

    const {dispatch} = this.props

    // prepair the offset
    const offset = (page - 1) * this.state.showUnits

    // get users
    dispatch(getUnits(
      this.state.showUnits,  // limit
      offset, // offset
      filters[currentFilter], // filter
      'id', // sort by
      'asc', // sort direction
      search,
    )).then(() => {
      // change the Pagination number
      this.setState({currentPage: page})
      this.setSession('currentPage', page)
    })

  }*/

  /***************************
  * Reload List
  ****************************/
 /* loadList = () => {
    this.setSession('currentFilter', this.getSession('currentFilter'))
    this.setSession('currentPage', this.getSession('currentPage'))
    this.setSession('showUnits', this.getSession('showUnits'))
    this.setSession('search', this.getSession('search'))

    const {filters} = this.state
    const offset = (this.getSession('currentPage') - 1) * this.getSession('showUnits')



    const {dispatch} = this.props
    dispatch(getUnits(
      this.getSession('showUnits'),
      offset,
      filters[this.getSession('currentFilter')],
      'id',
      'asc',
      this.getSession('search'))

    ).then(() => {
      this.setState({loading: false})
    })
}*/


  // render
  render(){

    // store props
    console.log(this.props)
    const {units, fetching, totalUnits, errMsg, current_user_permissions} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Units List',
      icon: (<LocationCity className="pagetitle-icon"/>),
      addbtn: true? "/units/add" : ""
    }

    // Flatslist Options
    const flatsListOptions = {
      units: units,
      loading: false,
      // update: fetching,
      // onPageChange: this.handlePagination,
      // total: totalUnits,
      // rowShowing: parseInt(this.getSession('showUnits')),
      // onChangeShowing: this.handleOnChangeShowing,
      // page: parseInt(this.getSession('currentPage')),
      // errorMsg: null,
      // showList: this.state.showList,
       onDelete: this.handleDelete,
      // onRestore: this.handleRestore,
      // onDisable: this.onDisable,
      // onEnable: this.onEnable,
      // filters: this.state.filters,
      // onChangeFilter: this.handleFilters,
      // onSearch: this.handleSearch,
      // search: this.getSession('search'),
      // currentFilter: this.getSession('currentFilter'),
    }
      
    return(
      <PagesContainer>

        {/* {this.state.disable_unit
            && <DisableUnitForm
                    obj={this}
                    unit_id={this.state.unit_id}
                    subunits={this.state.subunits}
                    reload={this.loadList} /> }

        {this.state.enable_unit
            && <EnableUnitForm
                    obj={this}
                    unit_id={this.state.unit_id}
                    subunits={this.state.subunits}
                    reload={this.loadList} /> }
 */}

        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['units']} />
          <PageHeader {...pageHeaderOptions} />
          <FlatsList {...flatsListOptions} permissions={current_user_permissions} />
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    units: state.UnitReducer.units,
    /*current_user_permissions: store.users.current_user_permissions,
    totalUnits: store.units.totalUnits,
    errMsg: store.units.errMsg,
    enableErrMsg: store.units.enableErrMsg,
    fetching: store.units.fetching,*/
  }
}

IndexUnits = connect(mapStateToProps)(IndexUnits)
export default IndexUnits
