/*
* IndexSpecialPrices - Component connected to redux
*/

// import main packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import swal from 'sweetalert'

// SpecialPrices Redux Actions
import {
  getSpecialPrices,
  deleteSpecialPrice,
  specialPricesSearch,
  applyFilter,
  restoreSpecialPrice} from '../../../../../Redux/actions/specialPricesActions'

import { getCurrentUserPermission } from '../../../../../Redux/actions/usersActions'

import {deleteItem} from '../../../../../Utilities/functions'

// Table row specialPrice
import RowSpecialPrice  from './rowSpecialPrice/RowSpecialPrice'
import {DummyRowSpecialPrice} from './rowSpecialPrice/DummyRowSpecialPrice'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  TableList} from '../../../../GlobalComponents/GlobalComponents'

// Material UI Icons
import LocationCity       from 'material-ui/svg-icons/social/location-city'


// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class IndexSpecialPrices extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      comID: 'specialPrices',
      showList : [
        {count: 10, name: '10'},
        {count: 15, name: '15'},
        {count: 30, name: '30'},
      ],
      // filters: [
      //   {value: 'false', params: 'soft_delete', name: 'All'},
      //   {value: 'true',params: 'soft_delete', name: 'Deleted'},
      //   {value: 'owner',params: 'role', name: 'Owners'},
      //   {value: 'normal', params: 'role', name: 'Normal'},
      //   {value: 'super admin', params: 'role', name: 'Super Admin'},
      //   {value: 'true', params: 'phone_verified', name: 'Phone Verified'},
      //   {value: true, params: 'email_verified', name: 'Email Verified'},
      // ],
      currentFilter: 0,
      currentPage: 1,
      showSpecialPrices: 10,
      sort_by: 'period_date',
      sort_direction: 'desc',
      search: ' ',

    }
    this.handlePagination   = this.handlePagination.bind(this)
    this.handleShowingSpecialPrices = this.handleShowingSpecialPrices.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    // this.handleFilters = this.handleFilters.bind(this)
    this.handleSorting = this.handleSorting.bind(this)
    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)
  }

  componentWillMount(){

    this.setSession('currentFilter', this.getSession('currentFilter'))
    this.setSession('currentPage', this.getSession('currentPage'))
    this.setSession('showSpecialPrices', this.getSession('showSpecialPrices'))
    this.setSession('sort_by', this.getSession('sort_by'))
    this.setSession('sort_direction', this.getSession('sort_direction'))
    this.setSession('search', this.getSession('search'))

    // const {filters, currentFilter} = this.state
    const offset = (this.getSession('currentPage') - 1) * this.getSession('showSpecialPrices')

    // On Load the Component get the specialPrices
    const {dispatch, match} = this.props

    const unit_id = match.params.id


    dispatch(getSpecialPrices(
      this.getSession('showSpecialPrices'),
      offset,
      // filters[this.getSession('currentFilter')],
      this.getSession('sort_by'),
      this.getSession('sort_direction'),
      this.getSession('search'),
      unit_id
      ),
    ).then(() => {

      // stop loading if successfull fetched the specialPrices
      this.setState({loading: false , unit_id: unit_id})
    })
    dispatch(getCurrentUserPermission())
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  /***************************
  * On Click Delete
  * @specialPriceid : (Intger)
  ****************************/
  handleDelete(specialPriceid){
    const {dispatch, errMsg, succMsg} = this.props
    let _this = this
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: 'Yes, Delete it!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    () => {
      dispatch(deleteSpecialPrice(specialPriceid)).then(res => {
        swal({
          title: 'Deleted',
          type: 'success',
          text: succMsg,
          timer: 2000,
          showConfirmButton: false
        })
      })
      .catch(err => {
        _this.forceUpdate()
        swal("Error", err.response.data.message, "error");
      })
    })

    //deleteItem(specialPriceid,dispatch, deleteSpecialPrice,this.props.errMsg)
  }

  handleRestoreSpecialPrice = (e, specialPrice_id) => {
    const {dispatch} = this.props
    e.preventDefault()
    dispatch(restoreSpecialPrice(specialPrice_id))
    .then(() => {
      swal({
        title: 'Restored',
        type: 'success',
        text: 'SpecialPrice has been restored successfully',
        timer: 2000,
        showConfirmButton: false
      })
    })
    //alert(specialPrice_id)
  }

  /***************************
  * On Click Pagination
  ****************************/
  handlePagination(e, page){
    e.preventDefault()
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    // prepair the offset
    const offset = (page - 1) * this.state.showSpecialPrices

    // get specialPrices
    dispatch(getSpecialPrices(
      this.state.showSpecialPrices,  // limit
      offset, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
      this.state.unit_id
    )).then(() => {
      // change the Pagination number
      this.setState({currentPage: page})
      this.setSession('currentPage', page)
    })

  }

  /***************************
  * Handle Sorting
  ****************************/
  handleSorting(sort_by){
    if(sort_by === 0) return false
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_direction} = this.state
    const {currentFilter, sort_direction} = this.state
    const sort_dir = (sort_direction === 'asc') ? 'desc' : 'asc'

    this.setState({
      sort_by: sort_by,
      sort_direction: sort_dir
    })
    this.setSession('sort_by', sort_by)
    this.setSession('sort_direction', sort_dir)
    this.setSession('currentPage', 1)

    dispatch(getSpecialPrices(
      this.getSession('showSpecialPrices'), // limit
      0, // offset
      // filters[this.getSession('currentFilter')], // filter
      this.getSession('sort_by'), // sort by
      this.getSession('sort_direction'), // sort direction
      this.getSession('search'),
      this.state.unit_id
    ))

  }


  /***************************
  * On Change Filter
  ****************************/
  // handleFilters(filter_type, value, index){
  //   const {dispatch} = this.props
  //   const {filters} = this.state
  //   this.setSession('currentFilter', index)
  //   this.setSession('currentPage', 1)
  //   //this.setState({currentFilter: index})
  //   dispatch(getSpecialPrices(
  //     this.state.showSpecialPrices, // limit
  //     0, // offset
  //     filters[index],// filter
  //     this.state.sort_by, //sort by
  //     this.state.sort_direction, // sort direction
  //     this.state.search,
  //   ))
  // }

  /***************************
  * On change showing specialPrices
  ****************************/
  handleShowingSpecialPrices(count){
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    dispatch(getSpecialPrices(
      count, // limit
      0, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
      this.state.unit_id
    )).then(() => {
      this.setState({showSpecialPrices: count, currentPage: 1})
      this.setSession('showSpecialPrices', count)
      this.setSession('currentPage', 1)
    })
  }

  /***************************
  * On search
  ****************************/
  handleSearch(e){
    const { dispatch , match } = this.props
    const length = e.target.value.length
    const s = e.target.value

    // const {filters, currentFilter, sort_by, sort_direction} = this.state
    const {sort_by, sort_direction , search } = this.state

    if(length >= 3 || length === 0){
      dispatch(getSpecialPrices(
        this.getSession('showSpecialPrices'),
        0,
        // filters[this.getSession('currentFilter')],
        this.getSession('sort_by'),
        this.getSession('sort_direction'),
        s,
        match.params.id
      ))

      this.setSession('search', s)
      this.setSession('currentPage', 1)
    }

  }

  // render
  render(){

    // Table columns
    const columns = [
      {params: 'id', title: 'ID'},
      {params: 'period_date', title: 'Period Date'},
      {params: 'price', title: 'Price'},
      {params: 0, title: 'Action'},
    ]
    // store data
    const {specialPrices, fetching, errMsg, totalSpecialPrices , match , current_user_permissions} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Special Prices List',
      icon: (<LocationCity className="pagetitle-icon"/>),
      addbtn: current_user_permissions.special_price_permissions.create ? "/units/"+match.params.id+"/special_price/add" : ''
    }

    // TableList Options
    const tableListOptions = {
      columns: columns,
      loading: this.state.loading,
      update: fetching,
      onPageChange: this.handlePagination,
      total: totalSpecialPrices,
      rowShowing: parseInt(this.getSession('showSpecialPrices')),
      onChangeShowing: this.handleShowingSpecialPrices,
      page: this.getSession('currentPage'),
      showList: this.state.showList,
      onSearch: this.handleSearch,
      search: this.getSession('search'),
      onError: errMsg,
      // filters: this.state.filters,
      // onChangeFilter: this.handleFilters,
      currentFilter: this.getSession('currentFilter'),
      onSorting: this.handleSorting
    }

    return(
      <PagesContainer path={this.props.location}>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['units','Special Prices']} />
          <PageHeader {...pageHeaderOptions} />
          <TableList {...tableListOptions}>

            { // if fetched specialPrices
              // !this.state.loading && specialPrices == null ?
              !this.state.loading ?
              (
                specialPrices.map(specialPrice => {
                  return (<RowSpecialPrice
                              key={'specialPrice'+specialPrice.id}
                              specialPrice={specialPrice}
                              onDelete={this.handleDelete}
                              restoreSpecialPrice={this.handleRestoreSpecialPrice}
                              unitid={match.params.id}
                              permissions={current_user_permissions}
                              />)
                })
              )
              :
              (
                DummyRowSpecialPrice()
              )
            /* end if */ }

          </TableList>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    specialPrices: store.specialPrices.specialPrices,
    current_user_permissions: store.users.current_user_permissions,
    totalSpecialPrices: store.specialPrices.totalSpecialPrices,
    errMsg: store.specialPrices.errMsg,
    fetching: store.specialPrices.fetching,
  }
}

IndexSpecialPrices = connect(mapStateToProps)(IndexSpecialPrices)
export default IndexSpecialPrices
