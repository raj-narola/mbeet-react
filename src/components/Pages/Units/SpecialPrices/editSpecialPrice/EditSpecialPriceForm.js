/*
* AddSpecialPriceForm: Object
* Child of: AddSpecialPrice
*/

// Main Packages
import React, {Component} from 'react'
// External Packages

// Material UI
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import RaisedButton from 'material-ui/RaisedButton';

class EditSpecialPriceForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      form: {},
    }

    this.handleChangeField = this.handleChangeField.bind(this)
  }

  handleChangeField(event){
    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
  }

  render(){
    const {specialPrice} = this.props;

    return(
      <form onSubmit={this.props.handleSubmit}>

        <div className="form-field">
          <DatePicker
              required
              floatingLabelText="Starting Date"
              hintText="Period Date"
              container="inline"
              name="period_date"
              mode="landscape"
              defaultDate={new Date(specialPrice.period_date)}
              style={{color: 'red'}} />
        </div>

        <div className="form-field">
           <TextField
            required
            floatingLabelText="Price"
            hintText="Price"
            style={{width: '100%'}}
            name="price"
            defaultValue={specialPrice.price}
          />
        </div>

        <div className="form-field submit">
          <RaisedButton label="Update SpecialPrice" type="submit" primary={true} />
        </div>

      </form>
    )
  }

}

export default EditSpecialPriceForm;
