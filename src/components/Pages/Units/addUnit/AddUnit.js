/*
* AddUnit: Compnent
* Child of: Units Container
*/

// Main Packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

/*import {addUnit, clearMsg} from '../../../../Redux/actions/unitsActions'*/
/*import {cloudinaryUpload, imagePath} from '../../../../Utilities/cloudinaryUpload'*/
/*import {getUser} from '../../../../actions/user';*/
/*import {getCities} from '../../../../Redux/actions/citiesActions'*/
/*import {getRules, getAmenities } from '../../../../Redux/actions/unitsActions'*/
import {addUnit} from '../../../../actions/Unit';
import {initUnit} from '../../../../actions/Unit';
// Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  FilterToolbar,
  FlatsList,
  Loading,
  AlertMsg,} from '../../../GlobalComponents/GlobalComponents'

import Form from '../Form/Form'


// Material UI Icons
import LocationCity       from 'material-ui/svg-icons/social/location-city'
import swal from 'sweetalert'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'
import serializeForm from 'form-serialize'


class AddUnit extends Component{

  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      images: [],
      loading: false,
    }

    this.handleImages = this.handleImages.bind(this)
  }

  handleImages(images){
    this.setState({images})
  }

  handleSubmit(e){
    e.preventDefault()
    
    const {dispatch} = this.props
    console.log(this.props)
    const {images} = this.state
    let obj = e.target
    let formData = serializeForm(e.target, { hash: true })
    console.log(e.target)
    console.log("\n=================================\n",formData,"\n=================================\n")
        
    dispatch(addUnit(formData))
    .then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "Unit added successfully",
        type: "success",
      }
      );
    })
    .catch(() => {
      swal("Sorry", "error adding units", "error");
      })
  }


  componentWillMount(){
    const {dispatch, match} = this.props
    dispatch(initUnit())
   /* dispatch(getUser(100000, 0, {params: 'role', value:'owner'}))*/
  /*  dispatch(getCities(10000))*/
    /*dispatch(getRules())*/
   /* dispatch(getAmenities())*/
    /*dispatch(getCurrentUserPermission())*/
  }


  // render
  render(){

    // Store props
    const {fetching, errMsg, succMsg, users, cities, rules, amenities, current_user_permissions} = this.props
    const {loading} = this.state

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Add New Unit',
      icon: (<LocationCity className="pagetitle-icon"/>),
    }

    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['units','Add New Unit']} />
          <PageHeader {...pageHeaderOptions} />
          <div className="page-container">

            {/* {(
                fetching
                || (!users || !cities || !rules || !amenities)
                || succMsg
                || loading)
            && <Loading success={succMsg} />} */}


              <Form
                formType="add"
                handleSubmit={this.handleSubmit}
                handleImages={this.handleImages}
                images={this.state.images}
                formType="add"
                users={users}
                cities={cities}
                rules={rules}
                amenities={amenities}

                 />


            {(errMsg) && <AlertMsg error={errMsg} />}

          </div>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}


const mapStateToProps = (store) => {
  return {
    errMsg: store.UnitReducer.errMsg,
    succMsg: store.UnitReducer.succMsg,
    fetching: store.UnitReducer.fetching,
    users: store.UnitReducer.users,
    cities: store.UnitReducer.cities,
    rules: store.UnitReducer.rules,
    amenities: store.UnitReducer.amenities,
    current_user_permissions: store.UnitReducer.current_user_permissions,
  }
}

AddUnit = connect(mapStateToProps)(AddUnit)
export default AddUnit
