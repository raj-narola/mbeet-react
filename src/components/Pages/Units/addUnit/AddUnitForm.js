/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import {getUsers} from '../../../../Redux/actions/usersActions'
import {getCities} from '../../../../Redux/actions/citiesActions'
import {getRules, getAmenities} from '../../../../Redux/actions/unitsActions'
import { ValidatorForm} from 'react-material-ui-form-validator'

// Components
import Step1 from './addUnitSteps/Step1'
import Step2 from './addUnitSteps/Step2'
import Step3 from './addUnitSteps/Step3'
import Step4 from './addUnitSteps/Step4'
import Step5 from './addUnitSteps/Step5'
/*import Step6 from './addUnitSteps/Step6'*/



// External Packages
import serializeForm from 'form-serialize'

// Style
import './addUnitStyle.css'

// Material UI
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

import {
  Step,
  Stepper,
  StepButton,
} from 'material-ui/Stepper';


class AddUnitForm extends Component {

  fields:array = [
    {step: 1, fields: ['user_id', 'title_en', 'title_ar', 'body_en', 'body_ar']},
    {step: 2, fields: ['price', 'service_charge', 'unit_type', 'unit_class', 'total_area']},
    {step: 3, fields: ['city']},
    {step: 4, fields: ['available_as', 'number_of_guests', 'number_of_rooms', 'number_of_beds', 'number_of_baths']},
    {step: 5, fields: ['rules']},
  ]

  constructor(props){
    super(props)

    this.state = {
      users: null,
      room_type: null,
      unit_type: null,
      available_as: null,
      city: null,
      rules: [],
      amenities: [],
      stepIndex: 0,
    }

    this.handleNextStep = this.handleNextStep.bind(this)
    this.handelFormErrors = this.handelFormErrors.bind(this)
  }

  componentWillMount(){
    const {dispatch} = this.props
    dispatch(getUsers(100000, 0, {params: 'role', value:'owner'}))
    dispatch(getCities(10000))
    dispatch(getRules())
    dispatch(getAmenities())
  }

  handleNextStep(e){
    alert(1)
  }

  handleNext = () => {
    const {stepIndex} = this.state;
    if (stepIndex < 5) {
      this.setState({stepIndex: stepIndex + 1});
    }
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };

  handelFormErrors(errors){
    errors.map(err => {
      this.fields.map(stepFields => {
        if(stepFields.fields.includes(err.props.name)){
          const step = stepFields.step - 1
          this.setState({stepIndex: step})
        }

      })

    })
  }




  // render
  render(){

    const {stepIndex, form} = this.state;
   
    const contentStyle = {margin: '0 16px'};
    const {users, cities, rules, amenities} = this.props

    return(
      <ValidatorForm
                  ref="form"
                  onSubmit={this.props.handleSubmit}
                  onError={errors => this.handelFormErrors(errors)}
                  className="add-unit-form">



      <Stepper linear={false} activeStep={stepIndex}>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 0})}>
            Unit Information
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 1})}>
            Unit Details
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 2})}>
            Location
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 3})}>
            Rooms Details
          </StepButton>
        </Step>

        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 4})}>
            Rules and Amenities
          </StepButton>
        </Step>

        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 5})}>
            Images
          </StepButton>
        </Step>
      </Stepper>
      <div style={contentStyle}>

        <Step1 onSubmit={this.handleNextStep} users={users} display={stepIndex === 0 ? true : false} />
        <Step2  display={stepIndex === 1 ? true : false} />
        <Step3 cities={cities} display={stepIndex === 2 ? true : false} />
        <Step4  display={stepIndex === 3 ? true : false} />
        <Step5  rules={rules} amenities={amenities} display={stepIndex === 4 ? true : false} />
        {/* <Step6  rules={rules} images={this.props.images} handleImages={this.props.handleImages} display={stepIndex === 5 ? true : false} /> */}

        <div style={{marginTop: 12}}>
          <FlatButton
            label="Back"
            disabled={stepIndex === 0}
            onTouchTap={this.handlePrev}
            style={{marginRight: 12}}
          />
          {stepIndex < 5 &&

            <RaisedButton
              label="Next"
              disabled={stepIndex === 5}
              primary={true}
              onTouchTap={this.handleNext}
            />}

          {stepIndex === 5 &&
            <RaisedButton
              label="Submit"

              primary={true}
              type="submit"
            />}
        </div>
      </div>


      </ValidatorForm>
    )
  }

}

const mapStateToProps = (store) => {
  return {
  /*  users: store.users.users,
    cities: store.cities.cities,
    rules: store.units.rules,
    amenities: store.units.amenities,*/
  }
}

AddUnitForm = connect(mapStateToProps)(AddUnitForm)
export default AddUnitForm
