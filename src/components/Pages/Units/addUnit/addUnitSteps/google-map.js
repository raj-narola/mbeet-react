import React, {Component} from 'react'
import {getLatLngFromData} from '../../../../../Utilities/functions.js'

import './googleMapSearchCss.css'

export default class GettingStartedExample extends Component {

  constructor(){
    super()

  }

  

  render() {
    return (
    <div style={{height: '400px'}}>
      <div className="pac-card" id="pac-card">
        <div>
          <div id="title">
            Search
          </div>
        </div>
        <br />
        <div id="pac-container">
          <input className="pac-input" id="pac-input" type="text" placeholder="Enter a location name" />
        </div>
        <div id="pac-container">
          <input className="pac-input" id="pac-url" type="text" placeholder="Enter URL" />
        </div>
        <div id="pac-container">
          <span className="err-msg" id="err-msg">Invaled URL</span>
        </div>
      </div>

     



    </div>

    );
  }
}
