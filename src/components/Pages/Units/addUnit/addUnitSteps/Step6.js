/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
import AddImgBtn from './AddImgBtn'
/*import {imagePath, pathToUrl, filePreview} from '../../../../../Utilities/cloudinaryUpload'*/


// External Packages
import $ from 'jquery'

class Step6 extends Component{
  constructor(props){
    super(props)
    this.handleUploadImgs = this.handleUploadImgs.bind(this)
    this.handleRemoveImg = this.handleRemoveImg.bind(this)

  }

  handleRemoveImg(index){
    const {handleImages} = this.props
    const images = this.props.images
    images.splice(index, 1)
    handleImages(images)
    this.forceUpdate()
  }

  handleUploadImgs(e){
    const files = e.target.files
    const {handleImages} = this.props

    this.setState({loading: true})

    let images = this.props.images

    filePreview(files)
    .then(imgs => {
      this.setState({loading: false})
      imgs.map(img => {
        images.push(img)
        handleImages(images)
        this.forceUpdate()
        //this.setState({images})
      })
    })


    /*
    cloudinaryUpload(files)
      .then(result => {

        let images = this.state.images
        result.map(img => {
          images.push(imagePath(img.url))
          this.setState({images})
        })

        this.setState({loading: false})

      })
    */
  }


  render(){
    const {display, images} = this.props

    return (
      <div style={{display: (display ? 'block' : 'none')}}>

        <div className="container" style={{padding: '50px 0'}}>
          <div className="row">
            {images.map((img, index) => {
              return (
                <div key={'img'+index} className="col-lg-3 col-md-3 col-sm-4 col-image-preview">

                    <span className="delete-prev-img" onClick={() => this.handleRemoveImg(index)}>x</span>
                    <img src={img} className="rounded img-fluid preview-img" />
                </div>
              )
            })}
            <div className="col-lg-3 col-md-3 col-sm-4">
              <AddImgBtn onSelectImg={this.handleUploadImgs} />
            </div>
          </div>
        </div>
      </div>
    )
  }

}

export default Step6
