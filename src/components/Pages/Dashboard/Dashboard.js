import React, { Component } from 'react'
import { connect } from 'react-redux'


// Global Components
import {
  PagesContainer,
} from '../../GlobalComponents/GlobalComponents'

import People from 'material-ui/svg-icons/social/people'

import './dashboard.css'

// Components
import { CSSTransitionGroup } from 'react-transition-group'

class Dashboard extends Component {

  render() {

    return (
      <PagesContainer path={this.props.location}>
        <div className="dashboard-container">
          <div className="row">
            <div className="col-lg-6 col-md-6">
              <div className="widget-container">

                <div className="dashboard-widget">
                  <div className="middle-box">
                    <span className="title">TOTAL REGISTERED USERS</span>
                    <span className="big-number">6</span>
                  </div>

                  <div className="row space-top">
                    <div className="col-lg-6 col-md-6 col-sm-6 center">
                      <span className="title-md">active</span>
                      <span className="number-md yellow">6</span>
                    </div>

                    <div className="col-lg-6 col-md-6 col-sm-6 center">
                      <span className="title-md">deleted</span>
                      <span className="number-md red">0</span>
                    </div>
                  </div>
                </div>


                <div className="dashboard-widget">
                  <div className="middle-box">
                    <span className="title">THIS MONTH BOOKINGS</span>
                    <span className="big-number">
                      0
                    </span>
                  </div>

                  <div className="row space-top">
                    <div className="col-lg-3 col-md-3 center">
                      <span className="title-md">pending</span>
                      <span className="number-md yellow">0</span>
                    </div>

                    <div className="col-lg-3 col-md-3 center">
                      <span className="title-md">cancelled</span>
                      <span className="number-md red">0</span>
                    </div>

                    <div className="col-lg-3 col-md-3 center">
                      <span className="title-md">confirmed</span>
                      <span className="number-md yellow">0</span>
                    </div>

                    <div className="col-lg-3 col-md-3 center">
                      <span className="title-md">completed</span>
                      <span className="number-md red">0</span>
                    </div>
                  </div>
                </div>

                
              </div>
            </div>
          </div>
        </div>
      </PagesContainer>

    )
  }
}

// Dashboard = connect(mapStateToProps)(Dashboard)
export default Dashboard
