import React from 'react'

const About = () => (
  <div>
    <div className="container">
      <div className="single-page-content">

        <div className="page-container">
          <h1 className="page-title">About</h1>
          <p>A digital platform that enables both investors and property owners to view their readily availableresidential units, which can be booked smoothlyin an innovative manner via the application as options that meet the needs of travelers from around the globe motivating them to use this platform.</p>
        </div>
      </div>
    </div>
  </div>
)
export default About
