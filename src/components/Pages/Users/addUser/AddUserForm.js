import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

// Material UI
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'

import { ValidatorForm, TextValidator, DateValidator} from 'react-material-ui-form-validator'
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import DatePicker from 'material-ui/DatePicker';
import Toggle from 'material-ui/Toggle';


class AddUserForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            form: {}
        }

        this.handleChange = this.handleChange.bind(this);
        this.hanldeEmailverified = this.hanldeEmailverified.bind(this);
        this.handlePhoneverify = this.handlePhoneverify.bind(this);
    }

    componentWillMount() {
        // custom rule will have name 'isPasswordMatch'
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            
            if (value !== this.state.form.password ) {
                return false;
            }
            return true;
        });

        ValidatorForm.addValidationRule('passwordLength', (value) => {

            if(value){
            if (value.length < 8) {
                return false;
            }
            return true;
        }
        });


        ValidatorForm.addValidationRule('Phone', (value) => {
           
            if (value){
                if (value.length == 10) {
                    return true;
                }
                return false;
            }
        });
        const { form } = this.state;
        form["gender"] = 0
        form["email_verified"] = false
        form["phone_verified"] = false
        this.setState({ form })
    }

    handleChange(event) {
        const { form } = this.state;
        form[event.target.name] = event.target.value
        this.setState({ form })
    }

    hanldeEmailverified (event, isInputChecked){
        console.log(isInputChecked)
        const { form } = this.state;
        form["email_verified"] = isInputChecked
        this.setState({ form })
    }

    handlePhoneverify(e,x) {
        const { form } = this.state;
        form["phone_verified"] = x
        this.setState({ form })
    }

    render() {
        
        const { onSubmit } = this.props
        const { form } = this.state

        return (
            <ValidatorForm
                ref="form"
                onSubmit={onSubmit}
            >
              <div className="form-field">
                <TextValidator
                    floatingLabelText="Firstname"
                    onChange={this.handleChange}
                    name="firstname"                    
                    value={form.firstname}
                    style={{ width: '100%' }}
                    validators={['required']}
                    errorMessages={['this field is required', 'first name is not valid']}
                />
              </div>

              <div className="form-field">
                <TextValidator
                    floatingLabelText="Lastname"
                    onChange={this.handleChange}
                    name="lastname"
                    value={form.lastname}
                    style={{ width: '100%' }}
                    validators={['required']}
                    errorMessages={['this field is required', 'last name is not valid']}
                />
              </div>  

              <div className="form-field">
                <TextValidator
                    floatingLabelText="Email"
                    onChange={this.handleChange}
                    name="email"
                    value={form.email}
                    style={{ width: '100%' }}
                    validators={['required', 'isEmail']}
                    errorMessages={['this field is required', 'email is not valid']}
                />
              </div>

              <div className="form-field">
                <TextValidator
                    floatingLabelText="Phone number"
                    onChange={this.handleChange}
                    name="phonenumber"
                    type="number"
                    value={String(form.phonenumber)}
                    style={{ width: '100%' }}
                    validators={['required', 'Phone']}
                    errorMessages={['this field is required', 'enter valid number']}
                />
              </div>

              <div className="form-field">

                <RadioButtonGroup name="gender" defaultSelected={0}  onChange={this.handleChange} value={form.gender}>
                    <RadioButton
                        value={0}
                        label="male"
                    />
                    <RadioButton
                        value={1}
                        label="female"
                    />
                </RadioButtonGroup>
              </div>


            <div className="form-field">
               
            <Toggle
              label="Email Verified"
              onToggle={this.hanldeEmailverified}
              />
              <input type="hidden" name="email_verified" value={form.email_verified} />
            </div>

            <div className="form-field">
            <Toggle
              label="Phone Verified"
              onToggle={this.handlePhoneverify}
              />
              <input type="hidden" name="phone_verified" value={form.phone_verified} />
              </div>

            <div className="form-field">
            <DateValidator

            hintText="Date of birth"
            container="inline"
            name="date_of_birth"
            mode="landscape"
            style={{color: 'red'}} />
              
            </div>    
                <div className="actions">
                    <RaisedButton type="submit" label="Submit" primary={true} className="submit-btn" />
                </div>
            </ValidatorForm>
        )
    }
}

// proptypes onSubmit
AddUserForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
}
export default AddUserForm


