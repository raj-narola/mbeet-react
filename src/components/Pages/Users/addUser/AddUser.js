import React,{Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'

import AddUserForm from './AddUserForm'

// Users Redux Actions
// import {addUser} from '../../../../Redux/actions/usersActions'
import swal from 'sweetalert'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

// Material UI Icons
import People from 'material-ui/svg-icons/social/people'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  AlertMsg,
  Loading} from '../../../GlobalComponents/GlobalComponents'

import serializeForm from 'form-serialize'
/*import { getRoles } from '../../../../Redux/actions/rolesActions'*/

import { addUser } from '../../../../actions/user';

class AddUser extends Component{

  constructor(props){
    super(props)
    this.state = {
      redirect: false
    }
    this.handleAddUser = this.handleAddUser.bind(this)
  }

  componentWillMount(){
    const {dispatch} = this.props
    /*dispatch(getRoles());*/
  }

  handleAddUser(e) {
    e.preventDefault()

    // get dispatch from props
    const { dispatch } = this.props

    // get form data
    console.log(e.target)
    let formData = serializeForm(e.target, { hash: true })
    let obj = e.target


    // dispatch
    console.log(formData)
    dispatch(addUser(formData))
    .then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "User added successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to users list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })
    .catch(() => {
      swal("Sorry", "This email address is already registered", "error");
      })
}



 /* handleSubmit(e){
    e.preventDefault()
    const {dispatch, succMsg} = this.props
    var formData = new FormData(e.target)
    let obj = e.target
    dispatch(addUser(formData)).then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "User added successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to users list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })

  }*/

  render(){

    // Store props
    const {errMsg, succMsg, fetching, roles} = this.props

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Add New User',
      icon: (<People className="pagetitle-icon"/>),
    }
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }
    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['users','add user']} />
          <PageHeader {...pageHeaderOptions} />

          <div className="page-container">

            {fetching && <Loading  />}
            {this.state.redirect && <Redirect to="/users" />}

            <div className="row justify-content-center">
              <div  className="col-lg-8 col-md-8" style={{position: 'inherit'}}>
                <AddUserForm onSubmit={this.handleAddUser} roles={roles} />
              </div>
            </div>

            {/* after dispatch, display the error or success message */}
            {(errMsg) && <AlertMsg error={errMsg} />}

          </div>

        </CSSTransitionGroup>
      </PagesContainer>
    )
  }

}

const mapStateToProps = (store) => {
  return {
    // roles: store.roles.roles,
    errMsg: store.UserReducer.errMsg,
    fetching: store.UserReducer.fetching,
    succMsg: store.UserReducer.succMsg,
  }
}

AddUser = connect(mapStateToProps, null)(AddUser)
export default AddUser
