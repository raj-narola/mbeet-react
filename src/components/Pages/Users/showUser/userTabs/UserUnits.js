import React,{Component} from 'react'
import {connect} from 'react-redux'

// Users Redux Actions
/*import {getUserUnits, deleteUnit, restoreUnit} from '../../../../../Redux/actions/usersActions'*/
import {getUserUnits} from '../../../../../actions/user'

import {deleteItem} from '../../../../../Utilities/functions'
// Components
import RowUnit from '../../../Units/indexUnits/rowUnit/RowUnit'
import DummyRowUnit from '../../../Units/indexUnits/rowUnit/DummyRowUnit'

import swal from 'sweetalert'

// Glonal Components
import {Loading} from '../../../../GlobalComponents/GlobalComponents'

class UserUnits extends Component{
  constructor(props){
    super(props)
    this.state = {
      loading: false,
    }
    this.handleDelete = this.handleDelete.bind(this)
    this.handleRestore = this.handleRestore.bind(this)
  }

  /***************************
  * On Click Delete
  * @id : (Intger)
  ****************************/
  handleDelete(e, id){
    e.preventDefault()
    const _this = this
    const {dispatch} = this.props
    deleteItem(id,dispatch, null,"Unit has some associated so can't be deleted", _this, 'Yes, delete it!', 'Moved to Trash')
  }

  /***************************
  * On Click Restore
  * @id : (Intger)
  ****************************/
  handleRestore(e, id){
    e.preventDefault()
    const {dispatch} = this.props
    // this.setState({loading: true})
    // dispatch(restoreUnit(id)).then(() => {
    //   this.forceUpdate()
    //   swal({
    //     title: "Restored Successfully",
    //     type: "success",
    //     timer: 2000,
    //     showConfirmButton: false
    //   })
    //   this.setState({loading: false})
    // })

  }

  componentWillMount(){
    const {dispatch, units} = this.props
    

  }

  render(){
    const {fetchingUnits,units, errMsg, permissions} = this.props

    return (
      <div className="page-container units-container">
        {this.state.loading && <Loading />}
        {(fetchingUnits || errMsg) ? (
          <div className="card-deck">
            <Loading error={errMsg} />
            <DummyRowUnit />
            <DummyRowUnit />
            <DummyRowUnit />
            <DummyRowUnit />
          </div>
        ):(
          units ? (
            <div className="card-deck">
              {units.map(unit => {
                return (<RowUnit
                  key={'unit'+unit.id}
                  unit={unit}
                  onDelete={this.handleDelete}
                  onRestore={this.handleRestore}
                  permissions={permissions}
                  />)
              })}

            </div>
          ):(
           
            <div className="alert alert-danger" style={{margin: '10px'}}>
              There are not units for this user
            </div>
          )
        )}




      </div>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    user_units: store.UserReducer.user_units,
    // errMsg: store.users.errMsg,
    // fetchingUnits: store.users.fetchingUnits,
  }
}

UserUnits = connect(mapStateToProps)(UserUnits)
export default UserUnits
