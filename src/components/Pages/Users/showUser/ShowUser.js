import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Link }           from 'react-router-dom'

// Users Redux Actions
// import {getUser,getCurrentUserPermission} from '../../../../Redux/actions/usersActions'
import {getUserUnits} from '../../../../actions/user'

import {paymentsStatus, leaseStatus} from '../../../../Utilities/options'

// dummy user
import DummyUser from './DummyUser'

// Material UI Icons
import Avatar from 'material-ui/Avatar';
import {Tabs, Tab} from 'material-ui/Tabs';
import Badge from 'material-ui/Badge';

// Material UI Icons
import People             from 'material-ui/svg-icons/social/people'
import LocationCityIcon   from 'material-ui/svg-icons/social/location-city'
import CreditCardIcon   from 'material-ui/svg-icons/action/credit-card'
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import AssignmentIndIcon       from 'material-ui/svg-icons/action/assignment-ind';
import ShoppingCartIcon       from 'material-ui/svg-icons/action/shopping-cart';
import FavoriteIcon       from 'material-ui/svg-icons/action/favorite';

/* get getShowUser from action */
import { getShowUser } from '../../../../actions/user';

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading} from '../../../GlobalComponents/GlobalComponents'

// Components
import Profile from './userTabs/Profile'
import UserUnits from './userTabs/UserUnits'

// External Packages
import ReactTooltip from 'react-tooltip'

// Style
import './showUserStyle.css'

class ShowUser extends Component{

  constructor(props){
    super(props)
  }

  componentWillMount(){

    const {dispatch, match} = this.props
    const userid = match.params.id
     dispatch(getShowUser(userid))
     dispatch(getUserUnits(userid))
    // dispatch(getCurrentUserPermission());
  }

  // render
  render(){

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // data props
    const {user,user_units, fetching, errMsg, current_user_permissions} = this.props
    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Show User',
      icon: (<People className="pagetitle-icon"/>),
    }

 

    return(
      <PagesContainer path={this.props.location}>

        <CSSTransitionGroup {...TrassiosnOptions}>
        <Breadcrumb path={['users','show user']} /> 
        <PageHeader {...pageHeaderOptions} />

          <div className="page-container clearfix">
            {(fetching || errMsg) &&
              <span>
                <Loading error={errMsg} />
                <DummyUser />
              </span>
            }

            {user ? (
                <div>
                <div className="row">
                  <div className="col-lg-6 col-md-6">
                    <div className="row">
                      <div className="col-lg-4 col-md-4">
                          <Avatar
                            src={/**(user.picture_attributes)*/false ? user.picture_attributes.name : 'https://www.w3schools.com/howto/img_avatar.png'}
                            size={140}
                            className="user-avatar"
                            />
                      </div>
                      <div className="col-lg-8 col-md-8">
                        <h3 className="user-name">
                            {user.first_name}
                          {/* <Link className="edit-user-btn" to={'/users/edit/'+user.id} >
                            <ModeEditIcon />
                          </Link> */}
                        </h3>
                        <span className="userrole">not assigned{/*user.role.name*/}</span>

                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-6 push-md-2">
                    <div className="statics">

                      <div className="statics-box">
                        <div data-tip="Units">
                          <span className="count" >{user_units==null ? 0 :user_units.length}</span>
                          <LocationCityIcon />
                        </div>
                      </div>

                      <div className="statics-box">
                        <div data-tip="Orders">
                          <span className="count">0{/*user.orders_count*/}</span>
                          <ShoppingCartIcon />
                        </div>
                      </div>

                      <div className="statics-box">
                        <div data-tip="Favorite">
                          <span className="count">0{/*user.unit_likes_count*/}</span>
                          <FavoriteIcon />
                        </div>
                      </div>


                      <div className="statics-box">
                        <span className="count">0</span>
                        <CreditCardIcon />
                      </div>

                    </div>
                  </div>
                </div>
                <div className="row user-tabs">
                  <div className="col-lg-12">
                    <Tabs className="tabs">
                      <Tab
                        icon={<AssignmentIndIcon className="tab-icon" />}
                        label="Profile">

                      <div className="tab-content">
                        <Profile user={user} />
                      </div>
                    </Tab>

                      
                        <Tab
                          icon={<LocationCityIcon className="tab-icon" />}
                          label="Unities">
                          <div className="tab-content">
                          {/* <div className="alert alert-danger">There are no units for this user</div> */}
                          
                          <UserUnits units={user_units} permissions={current_user_permissions}/> 

                          </div>
                        </Tab>
                      

                        <Tab
                          icon={<ShoppingCartIcon className="tab-icon" />}
                          label="Orders"
                        >

                          <div className="tab-content">
                            {false ? (
                              
                              {/* <table className="table orders">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Check In</th>
                                    <th>Check Out</th>
                                    <th>Payment Status</th>

                                    <th>Total Amount</th>
                                    <th>Created At</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {user.orders.map(order => {
                                    return (

                                      <tr key={'order'+order.id}>

                                        <td>
                                          <Link to={'/bookings/'+order.id}>
                                            {order.id}
                                          </Link>
                                        </td>
                                        <td>
                                          <Link to={'/bookings/'+order.id}>
                                            {dateFormat(new Date(order.check_in),'d-mm-yyyy h:MM TT')}
                                          </Link>
                                        </td>
                                        <td>
                                          <Link to={'/bookings/show/'+order.id}>
                                            {dateFormat(new Date(order.check_out),'d-mm-yyyy h:MM TT')}
                                          </Link>
                                        </td>
                                        <td>
                                          <Link to={'/bookings/'+order.id}>
                                            {paymentsStatus[order.payment_status]}
                                          </Link>
                                        </td>

                                        <td>
                                          <Link to={'/bookings/'+order.id}>
                                            SAR {order.total_amount}
                                          </Link>
                                        </td>
                                        <td>
                                          <Link to={'/bookings/'+order.id}>
                                            {dateFormat(new Date(order.created_at),'d-mm-yyyy h:MM TT')}
                                          </Link>
                                        </td>

                                      </tr>

                                    )
                                  })}
                                </tbody>
                              </table> */}
                            ):(
                              <div className="alert alert-danger">There are no bookings for this user</div>
                            )}

                          </div>
                        </Tab>
                        {/*}
                        <Tab
                          icon={<FavoriteIcon className="tab-icon" />}
                          label="Interests"
                        >
                        <div className="tab-content">
                          <div className="note note-danger"><p> This user not interested with any Unite ! </p></div>
                        </div>
                        </Tab>
                        */}
                      </Tabs>
                    </div>
                  </div>

              </div>)
            
            :          
            (null)
          }
          </div>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (state) => {
  console.log("=========",state.UserReducer.user)
  return {
    user: state.UserReducer.user,
    user_units: state.UserReducer.user_units,
    /*user: store.users.user,
    current_user_permissions: store.users.current_user_permissions,
    errMsg: store.users.errMsg,
    fetching: store.users.fetching,*/ 
  }
}

ShowUser = connect(mapStateToProps)(ShowUser)
export default ShowUser
