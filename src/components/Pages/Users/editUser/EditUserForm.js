/*
* AddUserForm: Object
* Child of: AddUser
*/

// Main Packages
import React, {Component} from 'react'
/*import {cloudinaryUpload, imagePath, pathToUrl} from '../../../../Utilities/cloudinaryUpload'*/
import {countryCode} from '../../../../Utilities/countries_code'
import {Loading} from '../../../GlobalComponents/GlobalComponents'
// External Packages
import serializeForm from 'form-serialize'
import PropTypes from 'prop-types'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';

import { ValidatorForm, TextValidator, DateValidator} from 'react-material-ui-form-validator'
class EditUserForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      form: {}
  }


  this.handleChange = this.handleChange.bind(this);
  this.hanldeEmailverified = this.hanldeEmailverified.bind(this);
  this.handlePhoneverify = this.handlePhoneverify.bind(this);
   
   /* this.handleUpload = this.handleUpload.bind(this)*/
    // this.handleCountryCode = this.handleCountryCode.bind(this)
    // this.hanldeEmailverified = this.hanldeEmailverified.bind(this)
    // this.hanldePhoneverified = this.hanldePhoneverified.bind(this)
  }

  componentWillMount(){

  //   ValidatorForm.addValidationRule('Phone', (value) => {
  //     console.log(value)
  //     console.log((String(value)).length)
  //     if (value){
  //         if ((value.length) == 10) {
  //             return true;
  //         }
  //         return false;
  //     }
  // });

  ValidatorForm.addValidationRule('Phone', (value) => {
           
    if (value){
        if (value.length == 10) {
            return true;
        }
        return false;
    }
    
});


    const {user} = this.props
    console.log(user)
    const { form } = this.state;
    form["first_name"] = user.first_name
    form["last_name"] = user.last_name
    form["phone"]=user.phone
    form["gender"]=user.gender
    form["email_verified"] = user.email_verified
    form["phone_verified"] = user.phone_verified
    form["email"]= user.email
    form["date_of_birth"]=user.date_of_birth
    this.setState({ form })


  }

  handleChange(event) {
    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
}

hanldeEmailverified (event, isInputChecked){
    console.log(isInputChecked)
    const { form } = this.state;
    form["email_verified"] = isInputChecked
    this.setState({ form })
}

handlePhoneverify(e,x) {
    const { form } = this.state;
    form["phone_verified"] = x
    this.setState({ form })
}
  
  

  render(){

    const { onSubmit } = this.props
    console.log(onSubmit)
    const { form } = this.state
    console.log(form)
    const {user} = this.props
   
    return(
      <ValidatorForm
       ref="form"
       onSubmit={this.props.handleSubmit} 
        >
        
        <div className="form-field">
         <TextValidator
          hintText="First Name"
          style={{width: '100%'}}
          name="first_name"
          value={form.first_name}
          defaultValue={form.first_name}
          onChange={this.handleChange}
          validators={['required']}
          errorMessages={['this field is required', 'first name is not valid']}          
        />
      </div>

      <div className="form-field">
         <TextValidator
          hintText="last Name"
          style={{width: '100%'}}
          name="last_name"
          value={form.last_name}
          defaultValue={form.last_name}
          onChange={this.handleChange}
          validators={['required']}
          errorMessages={['this field is required', 'last name is not valid']}          
        />
      </div>

      <div className="form-field">
        <TextValidator
         hintText="Email"
         style={{width: '100%'}}
         type="email"
         name="email"
         value={form.email}
         defaultValue={form.email} 
         onChange={this.handleChange}
         validators={['required']}
         errorMessages={['this field is required', 'Email is not valid']}             
       />
      </div>

      <div className="form-field">
     
       <TextValidator
          hintText="Phone"
          style={{width: '100%'}}
          type="number"
          name="phone"
          value={(form.phone)}
          defaultValue={(form.phone)}
          onChange={this.handleChange}
          validators={['required', 'Phone']}
          errorMessages={['this field is required', 'enter valid phone number']}          
       />
      </div>

      <div className="form-field">
      {console.log(form.date_of_birth)}
        <DateValidator
            hintText="Date of birth "
            container="inline"
            name="date_of_birth"
            mode="landscape"
            value={new Date(form.date_of_birth)}
            defaultDate	= {new Date(form.date_of_birth)}
            style={{color: 'red'}} />
      </div>


      <div className="form-field">
        <label>Gender</label>
        <RadioButtonGroup name="gender" value={form.gender} defaultSelected={form.gender} onChange={this.handleChange}>
            <RadioButton
              value={0}
              label="Male"
            />
            <RadioButton
              value={1}
              label="Female"
            />

        </RadioButtonGroup>
      </div>

      <div className="form-field">
          <Toggle
            label="Email Verified"
            defaultToggled={form.email_verified}
            onToggle={this.hanldeEmailverified}
            />
            <input type="hidden" name="email_verified" value={form.email_verified} />
      </div>

      <div className="form-field">
          <Toggle
            label="Phone Verified"
            defaultToggled={form.phone_verified}
            onToggle={this.hanldePhoneverified}
            />
            <input type="hidden" name="phone_verified" value={form.phone_verified} />
      </div>


      
      <div className="form-field submit">
        <RaisedButton label="Update" type="submit" primary={true} />
      </div>

      </ValidatorForm>
    )
  }

}

EditUserForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}
export default EditUserForm;
