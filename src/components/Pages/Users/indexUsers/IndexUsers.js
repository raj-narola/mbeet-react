/*
* IndexUsers - Component connected to redux
*/

// import main packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import swal from 'sweetalert'

import {deleteItem} from '../../../../Utilities/functions'

// Table row user
import RowUser  from './rowUser/RowUser'
import {DummyRowUser} from './rowUser/DummyRowUser'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  TableList} from '../../../GlobalComponents/GlobalComponents'

//action
import { deleteUser } from '../../../../actions/user';
//get user data
import { getUser } from '../../../../actions/user'

// Material UI Icons
import People from 'material-ui/svg-icons/social/people'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class IndexUsers extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      comID: 'users',
      showList : [
        {count: 10, name: '10'},
        {count: 15, name: '15'},
        {count: 30, name: '30'},
      ],
      filters: [
        {value: 'false', params: 'soft_delete', name: 'All'},
        {value: 'true',params: 'soft_delete', name: 'Deleted'},
        {value: 'owner',params: 'role', name: 'Owners'},
        {value: 'normal', params: 'role', name: 'Normal'},
        {value: 'super admin', params: 'role', name: 'Super Admin'},
        {value: 'true', params: 'phone_verified', name: 'Phone Verified'},
        {value: true, params: 'email_verified', name: 'Email Verified'},
      ],
      currentFilter: 0,
      currentPage: 1,
      showUsers: 10,
      sort_by: 'name',
      sort_direction: 'desc',
      search: ' ',

    }

    this.handleDelete = this.handleDelete.bind(this)
  }

  componentWillMount(){
    const {dispatch} = this.props
    dispatch(getUser())
    .then(resp => {
      console.log("in action", resp)
    })
  }

  
  handleDelete(userid){
    const {dispatch, errMsg, succMsg} = this.props
    let _this = this
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: 'Yes, Delete it!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    () => {
      dispatch(deleteUser(userid))
      .then(() => {
        swal({
          title: 'Deleted',
          type: 'success',
          text: "deleted",
          timer: 2000,
          showConfirmButton: false
        })
        // console.log("after delete")
        // dispatch(getUser())
      })
      .catch(err => {
        _this.forceUpdate()
        swal("Error", err.response.data.message, "error");
      })
    })

    //deleteItem(userid,dispatch, deleteUser,this.props.errMsg)
  }



 /*
handleDelete(userid){
 
  swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this imaginary file!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      swal("Poof! Your imaginary file has been deleted!", {
        icon: "success",
      });
    } else {
      swal("Your imaginary file is safe!");
    }
  });

  //deleteItem(userid,dispatch, deleteUser,this.props.errMsg)
}
*/

  
  
  // render
  render(){

    // Table columns
    const columns = [
      {params: 'id', title: 'ID'},
      {params: 'name', title: 'User'},
      {params: 'email', title: 'Email'},
      {params: 'phone', title: 'Phone'},
      {params: 'email_verified', title: 'Email Verified'},
      {params: 'phone_verified', title: 'Phone Verified'},
      {params: 0, title: 'Action'},
    ]
    // store data
    const {users, fetching, errMsg, totalUsers, current_user_permissions} = this.props
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Users List',
      icon: (<People className="pagetitle-icon"/>),
      addbtn: "/users/add"
    }

    // TableList Options
    const tableListOptions = {  
      columns: columns,
      loading: this.state.loading,
      update: fetching,
      onPageChange: this.handlePagination,
      total: totalUsers,
      rowShowing: 0/*parseInt(this.getSession('showUsers'))*/,
      onChangeShowing: this.handleShowingUsers,
      page: 0/*this.getSession('currentPage')*/,
      showList: this.state.showList,
      onSearch: this.handleSearch,
      search: null/*this.getSession('search')*/,
      onError: errMsg,
      filters: this.state.filters,
      onChangeFilter: this.handleFilters,
      currentFilter: null/*this.getSession('currentFilter')*/,
      onSorting: this.handleSorting
  }

    return(
      <PagesContainer path={this.props.location}>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['users']} />
          <PageHeader {...pageHeaderOptions} />
          <TableList {...tableListOptions}>

            {console.log("user==========", this.props)}
            {users  &&                
              users.map(user => {
                  return (<RowUser
                              key={'user'+user.id}
                              user={user}
                              onDelete={this.handleDelete}
                              restoreUser={this.handleRestoreUser}
                              /*permissions={current_user_permissions} *//>)
                })
           
            }
          </TableList>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.UserReducer.users,
    /*errMsg: state.LoginReducer.errMsg,
    current_user_permissions: store.users.current_user_permissions,
    totalUsers: store.users.totalUsers,
    errMsg: store.users.errMsg,
    fetching: store.users.fetching,*/
  }
}

IndexUsers = connect(mapStateToProps)(IndexUsers)
export default IndexUsers
