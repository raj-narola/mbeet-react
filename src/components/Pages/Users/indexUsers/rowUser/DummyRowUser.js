// Main Packages
import React from 'react';
import { Link }           from 'react-router-dom'

// Material UI
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';
import Avatar from 'material-ui/Avatar';

// Material UI Icons
import DoneIcon           from 'material-ui/svg-icons/action/done';
import ClearIcon          from 'material-ui/svg-icons/content/clear';
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import RemoveRedEyeIcon   from 'material-ui/svg-icons/image/remove-red-eye';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';

// Style
import './rowUser.css'

export function DummyRowUser () {
  var result = [];
  for (var i = 0; i < 5; i++) {
    result.push(
      <tr key={'dummy'+i}>
        <td width="250">
          <Avatar
              src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
              size={40}
              className="user-avatar"
              />
            <Link to="/users/view/1" className="username-with-avatar">
              <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
            </Link>
            <Link to="/users/view/1" className="username-with-avatar">
              <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
            </Link>
        </td>

        <td>
          <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
        </td>

        <td>
          <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
        </td>

        <td>
          <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
        </td>

        <td>
          <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
        </td>

        <td>
          <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
        </td>
        <td>
          <span style={{background: '#ccc', display: 'inline-block', width: '70%', height: '10px', borderRadius: '5px'}}></span>
        </td>
      </tr>
    )
  }

  return result
}
