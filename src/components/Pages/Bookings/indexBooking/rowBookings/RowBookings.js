import React from 'react'
import { Link }           from 'react-router-dom'

import {paymentsStatus} from '../../../../../Utilities/options'

import dateFormat  from 'dateformat'

// Material UI
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import RemoveRedEyeIcon   from 'material-ui/svg-icons/image/remove-red-eye';


const RowBooking = (props) => {
  const {booking, onCancel} = props
  return (
    <tr>
      <td>{booking.id}</td>
      <td>{booking.user.name}</td>
      <td>{booking.unit.title}</td>
      <td>{dateFormat(new Date(booking.check_in),'ddd, d mmm yyyy h:MM TT')}</td>
      <td>{dateFormat(new Date(booking.check_out),'ddd, d mmm yyyy h:MM TT')}</td>
      <td>{paymentsStatus[booking.payment_status]}</td>
      <td>
        {booking.payment_txn ? booking.payment_txn.transaction_id : '-'}
      </td>
      <td>SAR {booking.total_amount}</td>
      <td>{dateFormat(new Date(booking.created_at),'ddd, d mmm yyyy h:MM TT')}</td>
      <td>
        <IconMenu
           iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
           anchorOrigin={{horizontal: 'left', vertical: 'top'}}
           targetOrigin={{horizontal: 'left', vertical: 'top'}}
        >
          <Link to={'/bookings/'+booking.id} className="MenuItemBtn">
            <MenuItem primaryText="View" leftIcon={<RemoveRedEyeIcon />} />
          </Link>

          <MenuItem primaryText="Cancel" onClick={() => onCancel(booking.id)} leftIcon={<DeleteIcon />} />
        </IconMenu>
      </td>
    </tr>
  )
}

export default RowBooking
