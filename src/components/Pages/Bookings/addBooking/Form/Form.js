/*
* AddBookingForm: Compnent
* Child of: AddBooking
*/

// Main Packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import {getUsers} from '../../../../../Redux/actions/usersActions'
import {getCities} from '../../../../../Redux/actions/citiesActions'
import {addBooking} from '../../../../../Redux/actions/bookingsActions'
import { ValidatorForm} from 'react-material-ui-form-validator'

// Components
import Step1 from './addBookingSteps/Step1'
import Step2 from './addBookingSteps/Step2'
import Step3 from './addBookingSteps/Step3'

// External Packages
//import serializeForm from 'form-serialize'

// Style
import './addBookingStyle.css'

// Material UI
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

import {
  Step,
  Stepper,
  StepButton,
} from 'material-ui/Stepper';


class AddBookingForm extends Component {


  fields:array = [
    {step: 1, fields: ['user_id', 'city_id', 'lowest', 'highest','unit_type','number_of_beds']},
    {step: 2, fields: ['unit_id']},
    {step: 3, fields: ['PaymentTxn']},
  ]

  constructor(props){
    super(props)

    this.state = {
      users: null,
      room_type: null,
      booking_type: null,
      available_as: null,
      city: null,
      stepIndex: 2,
      form: {}
    }

    this.handelFormErrors = this.handelFormErrors.bind(this)
  }

  handleFildsValues = (id, value) => {
    const {form} = this.state
    form[id] = value
    this.setState({form})
    console.log(this.state.form);
  }



  handleNext = () => {
    const {stepIndex} = this.state;
    if (stepIndex < 3) {
      this.setState({stepIndex: stepIndex + 1});
    }
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };

  handelFormErrors(errors){
    errors.map(err => {
      this.fields.map(stepFields => {
        if(stepFields.fields.includes(err.props.name)){
          const step = stepFields.step - 1
          this.setState({stepIndex: step})
        }

      })

    })
  }


  // render
  render(){
    const {stepIndex, form} = this.state;
    const contentStyle = {margin: '0 16px'};
    const {booking, users, cities, rules, amenities, formType, permissions, units} = this.props

    // units.filter(unit => console.log(parseInt(form.lowest) < parseInt(unit.price) && parseInt(form.highest) > parseInt(unit.price) && unit));

    const st1 = (
                    form.user_id
                    && form.city_id
                    && form.lowest
                    && form.highest
                    && parseInt(form.lowest) <= parseInt(form.highest)
                    && form.unit_type
                  ) ? true : false
    const st2 = (
                    form.unit_id
                  ) ? true : false

    const st3 = (
                    form.PaymentTxn
                  ) ? true : false

    const canSubmit = (
      st1 && st2 && st3
    ) ? false : true

    const nextBtnActive = {
      0 : st1,
      1 : st2,
      2 : st3,
    }

    const unitFilter = units.filter(unit => parseInt(form.lowest? form.lowest : 0 ) < parseInt(unit.price) && parseInt(form.highest? form.highest : 10000000) > parseInt(unit.price) && parseInt(form.unit_type) === parseInt(unit.unit_type) && parseInt(form.number_of_beds) === parseInt(unit.number_of_beds) && unit);


    return(
      <ValidatorForm
                  ref="form"
                  onSubmit={this.props.handleSubmit}
                  onError={errors => this.handelFormErrors(errors)}
                  className="add-booking-form">



      <Stepper linear={false} activeStep={stepIndex}>
        <Step>
          <StepButton onClick={() => this.setState({stepIndex: 0})}>
            <span style={{color: (st1 ? 'green' : 'red')}}>Booking Information</span>
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => st1 ? this.setState({stepIndex: 1}) : false}>
            <span style={{color: (st2 ? 'green' : 'red')}}>Units</span>
          </StepButton>
        </Step>
        <Step>
          <StepButton onClick={() => st2 ? this.setState({stepIndex: 2}) : false}>
            <span style={{color: (st3 ? 'green' : 'red')}}>Payments</span>
          </StepButton>
        </Step>
      </Stepper>
      <div style={contentStyle}>

        <Step1 formType={this.props.formType} booking={booking} onChange={this.handleFildsValues}  users={users} display={stepIndex === 0 ? true : false}  cities={cities}/>
        <Step2 formType={this.props.formType} booking={booking} onChange={this.handleFildsValues} display={stepIndex === 1 ? true : false} permissions={permissions} units={unitFilter} />

        <Step3 formType={this.props.formType} booking={booking} onChange={this.handleFildsValues} display={stepIndex === 3 ? true : false} permissions={permissions} />

        <div style={{marginTop: 12}}>
          <FlatButton
            label="Back"
            disabled={stepIndex === 0}
            onTouchTap={this.handlePrev}
            style={{marginRight: 12}}
          />
        {stepIndex < 2 &&

            <RaisedButton
              label="Next"
              disabled={!nextBtnActive[stepIndex]}
              primary={true}
              onTouchTap={this.handleNext}
            />}


        </div>


      </div>

      <hr />
      <center>
        <RaisedButton
          label={formType === 'edit' ? 'Update' : 'Create'}
          style={{width: '50%'}}
          primary={true}
          disabled={canSubmit}
          type="submit"
        />
      </center>

      </ValidatorForm>
    )
  }

}

export default AddBookingForm
