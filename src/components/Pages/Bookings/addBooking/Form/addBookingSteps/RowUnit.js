/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

import { TextValidator, SelectValidator} from 'react-material-ui-form-validator'

import Avatar       from 'material-ui/Avatar';
import { Link }           from 'react-router-dom'
import ReactTooltip from 'react-tooltip'
import CheckedIcon  from 'material-ui/svg-icons/toggle/check-box'

import "./RowUnitStyle.css"

class RowUnit extends Component{
  constructor(props){
    super(props)

    this.state = {
      unit_id: ''
    }

  }

  render(){
    const {unit, permissions, handleSelectUnit, selected} = this.props
    const {form} = this.props
    const stars = [1,2,3,4,5]

    return(
      <div className='col-lg-3 unit-item-container' onClick={() => handleSelectUnit(unit)}
        style={{borderColor: (selected == unit.id ? '#ff6677' : '')}}
        >

        {selected == unit.id &&
          <div className="icons">
            <CheckedIcon />
          </div>
        }
        {unit.unit_status ? (
          <div className="unit-published">
            published
          </div>
        ):(
          <div className="waiting-for-approv">
            Waiting for approval
          </div>
        )}

        {unit.number_of_subunits > 1 &&
          <div className="subunits-count">
            <span>{parseInt(unit.number_of_subunits) - 1}</span>
            Subunits
          </div>
        }


        {unit.soft_delete &&
          <div className="on-trash">
            <div className="alrt">
              <div className="alert  alert-danger">
                <i className="fa fa-trash-o" aria-hidden="true"></i>
                On Trash
              </div>
            </div>
          </div> }

          <div className="card unit-item">

            <div className="card-img-top" style={{backgroundImage: `url(${unit.default_image})`}}>
              <img  className="img-fluid"
                src={(unit.images.length > 0) ? unit.images[0].url : unit.default_image}
                alt=""

                />
            </div>

            <div className="card-block">

              <div className="unit-owner">

                <Avatar size={50}
                  src="images/user-avatar.png"
                  className="owner-avatar"/>

                <span className="owner-name">
                  {unit.owner.name}
                </span>

              </div>

              <h6 className="card-title unit-title">
                {unit.title_en}
              </h6>

              <p className="card-text unit-address">
                <small>{unit.address}</small>
              </p>

              <p className="card-text unit-rating">
                {stars.map(star => {
                  const starClass = (star <= unit.avg_star) ? 'fa fa-star' : 'fa fa-star-o'
                  return (<i key={'star'+star} className={starClass} aria-hidden="true"></i>)
                })}
              </p>
              <div className="unit-price">
                SAR {unit.price}
              </div>



              <div className="col-lg-12 unit-details">
                <div className="row">
                  <div className="col-lg-6 col-md-6 unit-details-item">
                    <i className="mdi mdi-account-multiple"></i>
                    {unit.number_of_guests} Guests
                  </div>

                  <div className="col-lg-6 col-md-6 unit-details-item">
                    <i className="mdi mdi-hotel" aria-hidden="true"></i>
                    {unit.number_of_beds} Beds
                  </div>

                  <div className="col-lg-6 col-md-6 unit-details-item">
                    <i className="fa fa-home"></i>
                    {unit.number_of_rooms} Rooms
                  </div>

                  <div className="col-lg-6 col-md-6 unit-details-item">
                    <i className="fa fa-bath" aria-hidden="true"></i>
                    {unit.number_of_baths} Baths
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
  }

}

export default RowUnit
