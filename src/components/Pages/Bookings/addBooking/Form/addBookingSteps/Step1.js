/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
import RaisedButton from 'material-ui/RaisedButton';

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { TextValidator, SelectValidator, AutoCompleteValidator} from 'react-material-ui-form-validator'

import AutoComplete from 'material-ui/AutoComplete';

import '../addBookingStyle.css';

class Step1 extends Component{

  constructor(props){
    super(props)
    this.state = {
      user_id: '',
      searchTextCity: '',
      searchTextUser: '',
      form: {},
      dataSourceConfig: 'name',
      city_id: '',
      unit_type: '',
      number_of_beds: ''
    }

    this.handleChangeField = this.handleChangeField.bind(this)
    this.handleChangeUserID = this.handleChangeUserID.bind(this)
  }

  handleSelectUnitType     = (event, index, value) => {
    this.setState({unit_type: value})
    this.props.onChange('unit_type', value)
  }

  handleSelectCity  = (city) => {
    this.setState({city_id: city.id, searchTextCity: city.name})
    this.props.onChange('city_id', city.id)
  }

  handleChangeUserID(user){
    this.setState({user_id: user.id, searchTextUser: user.name});
    this.props.onChange('user_id', user.id)
  }

  handleChangeUser = (s, dataSource) => {
    const obj = dataSource.filter(d => d.name === s || d.email === s || d.phone === s)[0]
    dataSource.filter(d => d.name === s && this.setState({dataSourceConfig: 'name'}) )[0]
    dataSource.filter(d => d.email === s && this.setState({dataSourceConfig: 'email'}) )[0]
    dataSource.filter(d => d.phone === s && this.setState({dataSourceConfig: 'phone'}) )[0]
    if(obj) this.setState({user_id: obj.id})
    else this.setState({user_id: ''})
    this.props.onChange('user_id', null)
  }

  handleChangeField(event){

    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
    this.props.onChange(event.target.name, event.target.value)

  }

  componentWillMount(){
    const {formType, unit} = this.props
    if(formType === 'edit'){
      this.setState({
        user_id: unit.owner.user_id,
        searchTextUser: unit.owner.name,
        city: unit.city.id,
        searchTextCity: unit.city.name,
        unit_type: unit.unit_type,
        number_of_beds: unit.number_of_beds,
      })
      this.props.onChange('user_id', unit.owner.user_id)
      this.props.onChange('city_id', unit.city.id)
      this.props.onChange('lowest', 0)
      this.props.onChange('highest', 0)
      this.props.onChange('unit_type', unit.unit_type)
      this.props.onChange('number_of_beds', unit.number_of_beds)
    }
  }

  render(){
    const {users, display, unit, cities} = this.props
    const {form,dataSourceConfig} = this.state
    return(
      <div style={{display: (display ? 'block' : 'none')}}>

        {/* select city */}
        {cities &&
        <div className="form-field">

          <AutoCompleteValidator
            floatingLabelText="Destination"
            filter={AutoComplete.caseInsensitiveFilter}
            dataSource={cities}
            dataSourceConfig={ {text: 'name', value: 'id'}  }
            value={this.state.city}
            searchText={this.state.searchTextCity}
            openOnFocus={true}
            fullWidth={true}
            onNewRequest={this.handleSelectCity}
            name="city"
            validators={['required']}
            errorMessages={['this field is required']}
          />

           <input style={{display: 'none'}} type="text" name="city_id" value={this.state.city} />
        </div> }

        {/* Select User */}
        {users &&
          <div className="form-field">

            <AutoCompleteValidator
              floatingLabelText="User"
              filter={AutoComplete.caseInsensitiveFilter}
              dataSource={users}
              dataSourceConfig={ {text: dataSourceConfig, value: 'id'}}
              value={this.state.user_id}
              searchText={this.state.searchTextUser}
              openOnFocus={true}
              fullWidth={true}
              onNewRequest={this.handleChangeUserID}
              onUpdateInput={this.handleChangeUser}
              name="user_id"
              validators={['required']}
              errorMessages={['this field is required']}
            />
            <input  name="user_id" value={this.state.user_id} type="text" className="hidden-input" />
          </div> }


        <div className="form-field budget-fields">
          <div>
            <label>Budget</label>
          </div>
          <div className="form-field">
           <TextValidator
               className="budget-item"
               floatingLabelText="Lowest"
               onChange={this.handleChangeField}
               name="lowest"
               value={form.lowest}
               type="number"
               style={{width: '100%'}}
               min={0}
               validators={['required', 'minNumber: 0']}
               errorMessages={['this field is required', 'Number of must be equal or greater zero']}
             />
         </div>
         <div className="form-field">
           <TextValidator
               className="budget-item"
               floatingLabelText="Highest"
               onChange={this.handleChangeField}
               name="highest"
               value={form.highest}
               style={{width: '100%'}}
               type="number"
               min={0}
               validators={['required', 'minNumber: 0']}
               errorMessages={['this field is required', 'Number of must be equal or greater zero']}
             />
        </div>
       </div>

       {/* Unit type */}
       <div className="form-field">

         <SelectValidator
            floatingLabelText="Select Unit Type"
            value={this.state.unit_type}
            onChange={this.handleSelectUnitType}
            name="unit_type"
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          >

            <MenuItem value={1} primaryText="Resort" />
            <MenuItem value={2} primaryText="Villa" />
            <MenuItem value={3} primaryText="Apartment" />
            <MenuItem value={4} primaryText="Shaleeh" />
            <MenuItem value={5} primaryText="Private Room" />
            <MenuItem value={6} primaryText="Shared Room" />
          </SelectValidator>
          <input type="hidden" name="unit_type" value={this.state.unit_type} />
       </div>

       {/* Number of Beds */}
       <div className="form-field">

       <TextValidator
           floatingLabelText="Number of Beeds"
           onChange={this.handleChangeField}
           name="number_of_beds"
           value={form.number_of_beds}
           style={{width: '100%'}}
           type="number"
           min={0}
           validators={['required', 'minNumber: 0']}
           errorMessages={['this field is required', 'Number of beds must be equal or greater zero']}
         />
       </div>


      </div>
    )
  }
}
export default Step1
