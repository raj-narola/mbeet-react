import React from 'react'
import { NavLink }           from 'react-router-dom'

const TopMenuList = () => {
  const isLogged = (sessionStorage.getItem('TOKEN')) ? true : false
  return (
    <ul className="navbar-nav mr-auto">
      <li className="nav-item">
        {!isLogged &&
          <NavLink to="/login" exact activeClassName="active">
          Login
        </NavLink>}
      </li>
      <li className="nav-item">
        <NavLink to="/about" exact activeClassName="active">
          About
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink to="/privacy" exact activeClassName="active">
          Privacy
        </NavLink>
      </li>
    </ul>
  )
}

export default TopMenuList
