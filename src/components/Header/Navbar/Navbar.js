import React, { Component } from 'react'

import { Link } from 'react-router-dom'
import NavCollapse from './navCollapse/NavCollapse'
import UserProfileBtn from './userProfileBtn/UserProfileBtn'
import { NavLink } from 'react-router-dom'

import './navStyle.css'

export default class Navbar extends Component {
  render() {
    const isLogged = (localStorage.getItem('TOKEN')) ? true : false
    console.log(isLogged)
    return (
      <nav className="navbar navbar-toggleable-md navbar-light bg-faded fixed-top">
        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <Link to="/" className="navbar-brand">
          <img alt="logo" src={require("./images/logo.png")} className="img-responsive" />
        </Link>
        {/* <NavCollapse />
        {isLogged && <UserProfileBtn />} */}
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              {!isLogged &&
                <NavLink to="/" exact activeClassName="active">
                  Login
                  </NavLink>}
            </li>
            <li className="nav-item">
              {!isLogged &&
                <NavLink to="/register" exact activeClassName="active">
                  Register
                  </NavLink>}
            </li>
            <li className="nav-item">
              <NavLink to="/about" exact activeClassName="active">
                About
                </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/privacy" exact activeClassName="active">
                Privacy
                </NavLink>
            </li>
          </ul>
        </div>
        {isLogged && <UserProfileBtn />}
        {/* <div><Link to='/'>Login</Link></div>
          <div><Link to='/register'>register</Link></div> */}
      </nav>
    )
  }
}
