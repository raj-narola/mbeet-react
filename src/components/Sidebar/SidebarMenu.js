import React, {Component} from 'react'
import { NavLink }           from 'react-router-dom'

// Material UI
import People             from 'material-ui/svg-icons/social/people'
import LocationCity       from 'material-ui/svg-icons/social/location-city'
import RoleIcon           from 'material-ui/svg-icons/social/group-add'
import DashboardIcon      from 'material-ui/svg-icons/action/dashboard'
import EventNoteIcon      from 'material-ui/svg-icons/notification/event-note'
import PlaceIcon          from 'material-ui/svg-icons/maps/place'
import LoyaltyIcon        from 'material-ui/svg-icons/action/loyalty'
import FlatButton         from 'material-ui/FlatButton'
import TopDestination     from 'material-ui/svg-icons/action/youtube-searched-for'
import BookingIcon        from 'material-ui/svg-icons/notification/event-available'
import LogIcon            from 'material-ui/svg-icons/action/track-changes'

class SidebarMenu extends Component{
  render(){
    const {permissions} = this.props
    return (
      <div className="sidebar-menu">

        <ul className="menu-list">
                <li>
                  <NavLink to="/" exact activeClassName="active">
                    <FlatButton
                      label="Dashboard"
                      icon={<DashboardIcon />}
                      />
                  </NavLink>
                </li>
          

                 <li>
                   <NavLink to="/users" activeClassName="active">
                     <FlatButton
                       label="Users"
                       icon={<People />}
                       />
                   </NavLink>
                 </li>
             

                 <li>
                   <NavLink to="/units" activeClassName="active">
                     <FlatButton
                       label="Units"
                       icon={<LocationCity />}
                       />
                   </NavLink>
                 </li>
           

                 <li>
                   <NavLink to="/bookings" activeClassName="active">
                     <FlatButton
                       label="Bookings"
                       icon={<EventNoteIcon />}
                       />
                   </NavLink>
                 </li>
           

                 <li>
                   <NavLink to="/coupons" activeClassName="active">
                     <FlatButton
                       label="Coupons"
                       icon={<LoyaltyIcon />}
                       />
                   </NavLink>
                 </li>
           

                 <li>
                   <NavLink to="/cities" activeClassName="active">
                     <FlatButton
                       label="Cities"
                       icon={<PlaceIcon />}
                       />
                   </NavLink>
                 </li>
           
                 <li>
                   <NavLink to="/roles" activeClassName="active">
                     <FlatButton
                       label="Role"
                       icon={<RoleIcon />}
                       />
                   </NavLink>
                 </li>

                  <li>
                   <NavLink to="/topDestination" activeClassName="active">
                     <FlatButton
                       label="Top Destination"
                       icon={<TopDestination />}
                       />
                   </NavLink>
                 </li>

           <li>
            <NavLink to="/newBooking" activeClassName="active">
              <FlatButton
                label="New Booking"
                icon={<BookingIcon />}
                />
            </NavLink>
          </li>

          <li>
           <NavLink to="/logs" activeClassName="active">
             <FlatButton
               label="Logs"
               icon={<LogIcon />}
               />
           </NavLink>
         </li>

        </ul>
      </div>
    )
  }
}
export default SidebarMenu
