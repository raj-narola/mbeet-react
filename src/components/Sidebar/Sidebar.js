import React, {Component} from 'react'

// Componenets
import SidebarMenu from './SidebarMenu'


// Style
import './sidebarStyle.css'

class Sidebar extends Component{
  render(){
    return (
    <div className="row sidebar">
      <SidebarMenu path={this.props.path} /> 
    </div>
    )
  }
}

export default Sidebar
