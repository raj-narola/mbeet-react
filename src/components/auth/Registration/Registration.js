// Import main packages
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'

// Login Action - Redux Action
import { initAuth, registerUser } from "../../../actions/login";

// Components
import LoginContainer from "../loginContainer/LoginContainer"
import RegistrationForm from './RegistrationForm'

// Global Components
import Loading from '../../GlobalComponents/Loading'
import AlertMsg from '../../GlobalComponents/AlertMsg'

// External Packages
import serializeForm from 'form-serialize'


class Register extends Component {

    constructor(props) {
        super(props)
        this.handleRegister = this.handleRegister.bind(this)
    }

    componentWillMount() {
        const { dispatch } = this.props
        dispatch(initAuth())
    }

    // On Login form submit
    handleRegister(e) {
        e.preventDefault()

        // get dispatch from props
        const { dispatch } = this.props

        // get form data
        console.log(e.target)
        let formData = serializeForm(e.target, { hash: true })


        // dispatch
        console.log(formData)
        dispatch(registerUser(formData))
    }

    render() {
        // get messages from Redux store
        const { errMsg, succMsg, fetching } = this.props
        const isLogged = (sessionStorage.getItem('TOKEN')) ? true : false

        return (

            <LoginContainer title="Register">

                {/* Loading */}
                {fetching && <Loading />}

                {/* after dispatch, display the error or success message */}
                {(errMsg || succMsg) && <AlertMsg error={errMsg} success={succMsg} />}

                {/* login form */}
                <RegistrationForm onSubmit={this.handleRegister} />

            </LoginContainer>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        errMsg: state.LoginReducer.errMsg,
        succMsg: state.LoginReducer.succMsg,
        fetching: state.LoginReducer.fetching,
    }
}


const RegisterApp = connect(mapStateToProps, null)(Register)
export default RegisterApp;
