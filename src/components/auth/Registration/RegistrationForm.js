import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

// Material UI
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'

import { ValidatorForm, TextValidator, DateValidator} from 'react-material-ui-form-validator'
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import DatePicker from 'material-ui/DatePicker';




class RegistrationForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            form: {}
        }

        this.handleChange = this.handleChange.bind(this);
     
    }

    componentWillMount() {
        // custom rule will have name 'isPasswordMatch'
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            
            if (value !== this.state.form.password ) {
                return false;
            }
            return true;
        });

        ValidatorForm.addValidationRule('passwordLength', (value) => {

            if(value){
            if (value.length < 8) {
                return false;
            }
            return true;
        }
        });


        ValidatorForm.addValidationRule('Phone', (value) => {
           
            if (value){
                if (value.length == 10) {
                    return true;
                }
                return false;
            }
        });
    }



    handleChange(event) {
        const { form } = this.state;
        form[event.target.name] = event.target.value
        this.setState({ form })
    }

    render() {
        
        const { onSubmit } = this.props
        const { form } = this.state

        return (
            <ValidatorForm
                ref="form"
                onSubmit={onSubmit}
            >

                <TextValidator
                    floatingLabelText="Firstname"
                    onChange={this.handleChange}
                    name="firstname"
                    value={form.firstname}
                    style={{ width: '100%' }}
                    validators={['required']}
                    errorMessages={['this field is required', 'first name is not valid']}
                />

                <TextValidator
                    floatingLabelText="Lastname"
                    onChange={this.handleChange}
                    name="lastname"
                    value={form.lastname}
                    style={{ width: '100%' }}
                    validators={['required']}
                    errorMessages={['this field is required', 'last name is not valid']}
                />

                <TextValidator
                    floatingLabelText="Email"
                    onChange={this.handleChange}
                    name="email"
                    value={form.email}
                    style={{ width: '100%' }}
                    validators={['required', 'isEmail']}
                    errorMessages={['this field is required', 'email is not valid']}
                />

                <TextValidator
                    floatingLabelText="Phone number"
                    onChange={this.handleChange}
                    name="phonenumber"
                    type="number"
                    value={form.phonenumber}
                    style={{ width: '100%' }}
                    validators={['required', 'Phone']}
                    errorMessages={['this field is required', 'enter valid number']}
                />

                <TextValidator
                    floatingLabelText="Password"
                    onChange={this.handleChange}
                    name="password"
                    type="password"
                    validators={['required', 'passwordLength']}
                    errorMessages={['this field is required', 'Password length require more then 7 characters']}
                    value={form.password}
                />
                <br />

                <TextValidator
                    floatingLabelText="Repeat password"
                    onChange={this.handleChange}
                    name="repeatPassword"
                    type="password"
                    validators={['isPasswordMatch', 'required']}
                    errorMessages={['password mismatch', 'this field is required']}
                    value={form.repeatPassword}
                />

                <RadioButtonGroup name="gender" defaultSelected={0}>
                    <RadioButton
                        value={0}
                        label="male"
                    />
                    <RadioButton
                        value={1}
                        label="female"
                    />
                </RadioButtonGroup>

                <DateValidator

              hintText="Date of birth"
              container="inline"
              name="date_of_birth"
              mode="landscape"
              style={{color: 'red'}} />
                
                <div className="actions">
                    <RaisedButton type="submit" label="Submit" primary={true} className="submit-btn" />
                </div>
            </ValidatorForm>
        )
    }
}

// proptypes onSubmit
RegistrationForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
}
export default RegistrationForm


