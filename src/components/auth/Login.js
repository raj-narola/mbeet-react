// Import main packages
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'

// Login Action - Redux Action
import { loginUser, initAuth } from "../../actions/login";

// Components
import LoginContainer from "./loginContainer/LoginContainer"
import LoginForm from './LoginForm'

// Global Components
import Loading from '../GlobalComponents/Loading'
import AlertMsg from '../GlobalComponents/AlertMsg'

// External Packages
import serializeForm from 'form-serialize'


class Login extends Component {

    constructor(props) {
        super(props)
        this.handleLogin = this.handleLogin.bind(this)
    }

    componentWillMount() {
        const { dispatch } = this.props
        dispatch(initAuth())
    }

    // On Login form submit
    handleLogin(e) {
        e.preventDefault()

        // get dispatch from props
        const { dispatch } = this.props

        // get form data
        let formData = serializeForm(e.target, { hash: true })


        // dispatch
        dispatch(loginUser(formData))
    }

    render() {
        // get messages from Redux store
        const { errMsg, succMsg, fetching } = this.props
        const isLogged = (localStorage.getItem('TOKEN')) ? true : false
        console.log(localStorage)
        return (

            <LoginContainer title="Login">

                {isLogged && <Redirect to="/" />}

                {/* Loading */}
                {fetching && <Loading />}

                {/* after dispatch, display the error or success message */}
                {(errMsg || succMsg) && <AlertMsg error={errMsg} success={succMsg} />}

                {/* login form */}
                <LoginForm onSubmit={this.handleLogin} />

            </LoginContainer>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        errMsg: state.LoginReducer.errMsg,
        succMsg: state.LoginReducer.succMsg,
        fetching: state.LoginReducer.fetching,
    }
}


const LoginApp = connect(mapStateToProps, null)(Login)
export default LoginApp;
