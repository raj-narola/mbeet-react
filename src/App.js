import React, { Component } from 'react';
import logo from './logo.svg';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

// Components
import MaterialUiProvider from './Theme/MaterialUiProvider'

// Style
import './Theme/style.css'

import $ from 'jquery'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import 'mdi/css/materialdesignicons.min.css'
import 'font-awesome/css/font-awesome.min.css'
import 'sweetalert/dist/sweetalert.min.js'
import 'sweetalert/dist/sweetalert.css'


import RootRouter from './routers/RootRouter';

class App extends Component {
  render() {
    return (
      <MaterialUiProvider>
          <BrowserRouter>
            <RootRouter />
          </BrowserRouter>
      </MaterialUiProvider>
    );
  }
}

export default App;
