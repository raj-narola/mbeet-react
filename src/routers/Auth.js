import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import Login from '../components/auth/Login';
import Logout from '../components/auth/Logout';
import Sidebar from '../components/Sidebar/Sidebar'
import Dashboard from '../components/Pages/Dashboard/Dashboard';
import privacy from '../components/Pages/Privacy/Privacy';
import About from '../components/Pages/About/About';
import IndexUsers from '../components/Pages/Users/indexUsers/IndexUsers';
import AddUser from '../components/Pages/Users/addUser/AddUser';
import ShowUser from '../components/Pages/Users/showUser/ShowUser';
import EditUser from '../components/Pages/Users/editUser/EditUser';
import IndexUnits from '../components/Pages/Units/indexUnits/IndexUnits';
import AddUnit from '../components/Pages/Units/addUnit/AddUnit';
import ShowUnit from '../components/Pages/Units/showUnit/ShowUnit';
import EditUnit from '../components/Pages/Units/editUnit/EditUnit';

const Auth = () => {
    return (
        <div className="row clearfix">
            <div className="h-100 col-lg-2 col-md-2">
                <Sidebar />
            </div>
            <Switch>
                <Route exact path="/" component={Dashboard} />
                <Route exact path="/privacy" component={privacy} />
                <Route exact path="/about" component={About} />
                <Route path="/users" exact component={IndexUsers} />
                <Route path="/users/add" exact component={AddUser} />
                <Route path="/users/show/:id" exact component={ShowUser} />
                <Route path="/users/edit/:id" exact component={EditUser} />
                <Route path="/units" exact component={IndexUnits} />
                <Route path="/units/add" exact component={AddUnit} />
                <Route path="/units/show/:id" exact component={ShowUnit} />
                <Route path="/units/edit/:id" exact component={EditUnit} />
            </Switch>
        </div>
    )
}

export default Auth