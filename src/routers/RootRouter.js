import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import  { Redirect } from 'react-router-dom'


import NoAuth from './NoAuth';
import Auth from './Auth';
import Navbar from '../components/Header/Navbar/Navbar'

const RootRouter = () => {
    const token = localStorage.TOKEN
    return (
        <div>
        <Navbar />
            {token ?
                <Auth />
            :
                <NoAuth />
            }
        </div>
    )
}

export default RootRouter