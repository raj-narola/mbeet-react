import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import Login from '../components/auth/Login';
import Register from '../components/auth/Registration/Registration';
import privacy from '../components/Pages/Privacy/Privacy';
import About from '../components/Pages/About/About';

const NoAuth = () => {
    return( 
        <Switch>
            <Route exact path="/register" component={Register} />
            <Route exact path="/privacy" component={privacy} />
            <Route exact path="/about" component={About} />
            <Route component={Login} />
            
            
        </Switch>
    )
}

export default NoAuth