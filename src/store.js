import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers/index";
import thunk from "redux-thunk";
import promise from 'redux-promise-middleware'
import { createLogger } from 'redux-logger'
 
let logger = []


if (process.env.NODE_ENV === 'development') {
  logger.push(createLogger())
}

const middleware = applyMiddleware(promise(), thunk, ...logger)


const store = createStore(
rootReducer,
  middleware,
)

export default store
