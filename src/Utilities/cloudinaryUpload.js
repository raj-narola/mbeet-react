import axios from 'axios'
import {current_env} from '../env'

export function imagePath(url){
  let path = url.replace('http://res.cloudinary.com/mbeetapp/image/upload/', '')
  path = path.replace('http://res.cloudinary.com/clickapp/image/upload/', '')
  path = path.replace('https://res.cloudinary.com/mbeetapp/image/upload/', '')
  path = path.replace('https://res.cloudinary.com/clickapp/image/upload/', '')

  return path
}

export function pathToUrl(path){
  let url = (current_env == 'development')
              ? 'http://res.cloudinary.com/clickapp/image/upload/'+path
              : 'http://res.cloudinary.com/mbeetapp/image/upload/'+path
  return url
}

function getEnv(){
  const env = current_env[0]
  let API_URL, upload_preset = null
  API_URL = (env == 'development')
          ? 'https://api.cloudinary.com/v1_1/clickapp/image/upload'
          : 'https://api.cloudinary.com/v1_1/mbeetapp/image/upload'

  switch (env) {
    case 'development':
      upload_preset = 'mbeet-dev'
      break;

    case 'staging':
      upload_preset = 'mbeet-staging'
      break;

    case 'production':
      upload_preset = 'mbeet-prod'
      break;

    default:
      upload_preset = 'mbeet-dev'
  }

  return {upload_preset, API_URL}
}



export function cloudinaryUpload(files, readDataURL = true) {

    const upload_preset = getEnv().upload_preset
    const API_URL       = getEnv().API_URL

    return new Promise((resolve, reject) => {

      let result = []
      if(readDataURL){
        Object.keys(files).map(index => {

          const file = files[index]

            getDataURL(file).then(img => {
              fetchToCloudinary(img, API_URL, upload_preset)
              .then(response => {
                result.push(response.data)

                if(result.length === files.length)
                  resolve(result)
              })
            })

        })
      }else{
        files.map(img => {
          fetchToCloudinary(img, API_URL, upload_preset)
          .then(response => {
            result.push(response.data)

            if(result.length === files.length)
              resolve(result)
          })
        })
      }

    })
}

export function getDataURL(file){

  return new Promise((resolve, reject) => {

    const Reader = new FileReader()
    Reader.readAsDataURL(file)

    Reader.onload = e => {
      resolve(e.target.result)
    }

  })
}

function fetchToCloudinary(file, API_URL, upload_preset){
  return new Promise((resolve, reject) => {

    const data = {
      file: file,
      upload_preset: upload_preset,
    }

    axios.post(API_URL, data)
    .then(response => {
      resolve(response)
    })

  })
}




export function filePreview(files){

  return new Promise((resolve, reject) => {

    let result = []
    Object.keys(files).map(index => {

      const file = files[index]

      const Reader = new FileReader()
      Reader.readAsDataURL(file)
      Reader.onload =  e => {
        result.push(e.target.result)
        if(result.length === files.length)
          resolve(result)
      }

    })

  })
}
