var axios = require('axios')
var axiosDefaults = require("axios/lib/defaults");
axiosDefaults.xsrfCookieName = "csrftoken"
axiosDefaults.xsrfHeaderName = "X-CSRFToken"
axios.defaults.headers.post['Content-Type'] = 'application/json'



const API_VERSION = 'v6'

const defaultHeader = {
  'Content-Type': 'application/json',
  xsrfHeaderName: "X-CSRFToken",
}

function handleAPI(url, method, data = null, Authorization = false, headers = defaultHeader) {
  url = url

  if (Authorization) Object.assign(headers, Authorization);
  console.log("in handleAPI")
  return axios({
    method,
    url,
    data,
    headers,
  })
}

export {
  handleAPI,
  API_VERSION
}
