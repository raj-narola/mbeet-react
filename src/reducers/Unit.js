const initialState = {
    units: null,
    unit: null,
    rules: null,
    amenities: null,
    totalUnits: 0,
    errMsg: null,
    succMsg: null,
    fetching: false,
    city: null,
};


let changes = null
export default function UnitReducer(state = initialState, action) {
    switch (action.type) {

        case "INIT_UNIT_PENDING":
            return { ...state, fetching: true, errMsg: null, succMsg: null }

        case "INIT_UNIT_FULFILLED":
            return { ...state, fetching: false }
        
        case "FETCH_UNIT":
        if (action.error) {
            return { ...state}
        }
        else {
            changes = {
                unit:action.resp.data['unit'][0]
            }
            console.log("in reducer ",action.resp.data)
            return { ...state, ...changes }
        }
        
        case "FETCH_UNITS":
            if (action.error) {
                return { ...state}
            }
            else {
                changes = {
                    units:action.resp.data['units']
                }
                console.log("in reducer ",action.resp.data)
                return { ...state, ...changes }
            }
        case "DELETE_UNIT":
            console.log("---------------------",state)
            console.log(action.unitid)
            changes = {
                units : state.units.filter(unit => unit.id != action.unitid)
            }
          return { ...state, ...changes }
    }
    return state
}