import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import LoginReducer from './login'
import UserReducer from'./User'
import UnitReducer from'./Unit'


const rootReducer = combineReducers({
  form: formReducer,
  UnitReducer,
  LoginReducer,
  UserReducer
})

export default rootReducer;