const initialState = {
    errMsg: null,
    succMsg: null,
    fetching: false,
};

let changes = null


export default function UserReducer(state = initialState, action) {
    switch (action.type) {
        case 'FETCH_USER':
            if (action.error) {
                return { ...state}
            }
            else {
                changes = {
                    users:action.resp.data['users']
                }
                console.log("in reducer ",action.resp.data)
                return { ...state, ...changes }
            }
        case 'SHOW_USER':
            if (action.error) {
                return {...state}
            }
            else {
                changes = {
                    user:action.resp.data['users'][0]
                }
                console.log("in reducer ",action.resp.data)
                return { ...state, ...changes }
            }    
        case "DELETE_USER":
            console.log("---------------------",state)
            console.log(action.userid)
            changes = {
                units : state.users.filter(user => user.id != action.userid)
            }
            return { ...state, ...changes }
        case "GET_USER_UNIT":
            console.log(action.resp.data)
            changes = {
            user_units : action.resp.data['units']
            }
            return { ...state, ...changes }    
        case "NO_GET_USER_UNIT":
            changes = {
                user_units : null
                }
            return { ...state, ...changes }    
    }

    return state
}
