const initialState = {
    errMsg: null,
    succMsg: null,
    fetching: false,
};

let changes = null
export default function LoginReducer(state = initialState, action) {
    switch (action.type) {

        case "INIT_AUTH_PENDING":
            return { ...state, fetching: true, errMsg: null, succMsg: null }


        case 'LOGIN_USER':
            //console.log(action.resp)
            if (action.error) {
                changes = {
                    errMsg: action.error.response.data.error,
                    fetching: false,
                }
                return { ...state, ...changes }
            }
            else {
                if (action.resp.status == 200) {
                    localStorage.setItem('TOKEN', JSON.stringify(action.resp.data))
                    localStorage.setItem('password', action.password)
                    return [...state, { 'TOKEN': action.resp.token }]
                }
            }

        case 'REGISTER_USER':
            if (action.error) {
                changes = {
                    errMsg: action.error.response.data.error,
                    fetching: false,
                }
                return { ...state, ...changes }
            }
            else {
                //console.log(action.resp.data.success)
                changes = {
                    succMsg: action.resp.data.success,
                    fetching: false,
                }
                return { ...state, ...changes }
            }

        case "INIT_AUTH_FULFILLED":
            return { ...state, fetching: false }


    }
    return state
}